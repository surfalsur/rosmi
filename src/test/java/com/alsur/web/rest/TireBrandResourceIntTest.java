package com.alsur.web.rest;

import com.alsur.RosmiApp;

import com.alsur.domain.TireBrand;
import com.alsur.repository.TireBrandRepository;
import com.alsur.repository.search.TireBrandSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TireBrandResource REST controller.
 *
 * @see TireBrandResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RosmiApp.class)
public class TireBrandResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Inject
    private TireBrandRepository tireBrandRepository;

    @Inject
    private TireBrandSearchRepository tireBrandSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restTireBrandMockMvc;

    private TireBrand tireBrand;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TireBrandResource tireBrandResource = new TireBrandResource();
        ReflectionTestUtils.setField(tireBrandResource, "tireBrandSearchRepository", tireBrandSearchRepository);
        ReflectionTestUtils.setField(tireBrandResource, "tireBrandRepository", tireBrandRepository);
        this.restTireBrandMockMvc = MockMvcBuilders.standaloneSetup(tireBrandResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TireBrand createEntity(EntityManager em) {
        TireBrand tireBrand = new TireBrand()
                .name(DEFAULT_NAME)
                .code(DEFAULT_CODE);
        return tireBrand;
    }

    @Before
    public void initTest() {
        tireBrandSearchRepository.deleteAll();
        tireBrand = createEntity(em);
    }

    @Test
    @Transactional
    public void createTireBrand() throws Exception {
        int databaseSizeBeforeCreate = tireBrandRepository.findAll().size();

        // Create the TireBrand

        restTireBrandMockMvc.perform(post("/api/tire-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tireBrand)))
            .andExpect(status().isCreated());

        // Validate the TireBrand in the database
        List<TireBrand> tireBrandList = tireBrandRepository.findAll();
        assertThat(tireBrandList).hasSize(databaseSizeBeforeCreate + 1);
        TireBrand testTireBrand = tireBrandList.get(tireBrandList.size() - 1);
        assertThat(testTireBrand.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTireBrand.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the TireBrand in ElasticSearch
        TireBrand tireBrandEs = tireBrandSearchRepository.findOne(testTireBrand.getId());
        assertThat(tireBrandEs).isEqualToComparingFieldByField(testTireBrand);
    }

    @Test
    @Transactional
    public void createTireBrandWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tireBrandRepository.findAll().size();

        // Create the TireBrand with an existing ID
        TireBrand existingTireBrand = new TireBrand();
        existingTireBrand.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTireBrandMockMvc.perform(post("/api/tire-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingTireBrand)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TireBrand> tireBrandList = tireBrandRepository.findAll();
        assertThat(tireBrandList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTireBrands() throws Exception {
        // Initialize the database
        tireBrandRepository.saveAndFlush(tireBrand);

        // Get all the tireBrandList
        restTireBrandMockMvc.perform(get("/api/tire-brands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tireBrand.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getTireBrand() throws Exception {
        // Initialize the database
        tireBrandRepository.saveAndFlush(tireBrand);

        // Get the tireBrand
        restTireBrandMockMvc.perform(get("/api/tire-brands/{id}", tireBrand.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tireBrand.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTireBrand() throws Exception {
        // Get the tireBrand
        restTireBrandMockMvc.perform(get("/api/tire-brands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTireBrand() throws Exception {
        // Initialize the database
        tireBrandRepository.saveAndFlush(tireBrand);
        tireBrandSearchRepository.save(tireBrand);
        int databaseSizeBeforeUpdate = tireBrandRepository.findAll().size();

        // Update the tireBrand
        TireBrand updatedTireBrand = tireBrandRepository.findOne(tireBrand.getId());
        updatedTireBrand
                .name(UPDATED_NAME)
                .code(UPDATED_CODE);

        restTireBrandMockMvc.perform(put("/api/tire-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTireBrand)))
            .andExpect(status().isOk());

        // Validate the TireBrand in the database
        List<TireBrand> tireBrandList = tireBrandRepository.findAll();
        assertThat(tireBrandList).hasSize(databaseSizeBeforeUpdate);
        TireBrand testTireBrand = tireBrandList.get(tireBrandList.size() - 1);
        assertThat(testTireBrand.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTireBrand.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the TireBrand in ElasticSearch
        TireBrand tireBrandEs = tireBrandSearchRepository.findOne(testTireBrand.getId());
        assertThat(tireBrandEs).isEqualToComparingFieldByField(testTireBrand);
    }

    @Test
    @Transactional
    public void updateNonExistingTireBrand() throws Exception {
        int databaseSizeBeforeUpdate = tireBrandRepository.findAll().size();

        // Create the TireBrand

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTireBrandMockMvc.perform(put("/api/tire-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tireBrand)))
            .andExpect(status().isCreated());

        // Validate the TireBrand in the database
        List<TireBrand> tireBrandList = tireBrandRepository.findAll();
        assertThat(tireBrandList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTireBrand() throws Exception {
        // Initialize the database
        tireBrandRepository.saveAndFlush(tireBrand);
        tireBrandSearchRepository.save(tireBrand);
        int databaseSizeBeforeDelete = tireBrandRepository.findAll().size();

        // Get the tireBrand
        restTireBrandMockMvc.perform(delete("/api/tire-brands/{id}", tireBrand.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean tireBrandExistsInEs = tireBrandSearchRepository.exists(tireBrand.getId());
        assertThat(tireBrandExistsInEs).isFalse();

        // Validate the database is empty
        List<TireBrand> tireBrandList = tireBrandRepository.findAll();
        assertThat(tireBrandList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTireBrand() throws Exception {
        // Initialize the database
        tireBrandRepository.saveAndFlush(tireBrand);
        tireBrandSearchRepository.save(tireBrand);

        // Search the tireBrand
        restTireBrandMockMvc.perform(get("/api/_search/tire-brands?query=id:" + tireBrand.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tireBrand.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }
}
