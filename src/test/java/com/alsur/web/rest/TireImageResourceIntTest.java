package com.alsur.web.rest;

import com.alsur.RosmiApp;

import com.alsur.domain.TireImage;
import com.alsur.repository.TireImageRepository;
import com.alsur.repository.search.TireImageSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TireImageResource REST controller.
 *
 * @see TireImageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RosmiApp.class)
public class TireImageResourceIntTest {

    private static final String DEFAULT_IMAGE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_PATH = "BBBBBBBBBB";

    @Inject
    private TireImageRepository tireImageRepository;

    @Inject
    private TireImageSearchRepository tireImageSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restTireImageMockMvc;

    private TireImage tireImage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TireImageResource tireImageResource = new TireImageResource();
        ReflectionTestUtils.setField(tireImageResource, "tireImageSearchRepository", tireImageSearchRepository);
        ReflectionTestUtils.setField(tireImageResource, "tireImageRepository", tireImageRepository);
        this.restTireImageMockMvc = MockMvcBuilders.standaloneSetup(tireImageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TireImage createEntity(EntityManager em) {
        TireImage tireImage = new TireImage()
                .imagePath(DEFAULT_IMAGE_PATH);
        return tireImage;
    }

    @Before
    public void initTest() {
        tireImageSearchRepository.deleteAll();
        tireImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createTireImage() throws Exception {
        int databaseSizeBeforeCreate = tireImageRepository.findAll().size();

        // Create the TireImage

        restTireImageMockMvc.perform(post("/api/tire-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tireImage)))
            .andExpect(status().isCreated());

        // Validate the TireImage in the database
        List<TireImage> tireImageList = tireImageRepository.findAll();
        assertThat(tireImageList).hasSize(databaseSizeBeforeCreate + 1);
        TireImage testTireImage = tireImageList.get(tireImageList.size() - 1);
        assertThat(testTireImage.getImagePath()).isEqualTo(DEFAULT_IMAGE_PATH);

        // Validate the TireImage in ElasticSearch
        TireImage tireImageEs = tireImageSearchRepository.findOne(testTireImage.getId());
        assertThat(tireImageEs).isEqualToComparingFieldByField(testTireImage);
    }

    @Test
    @Transactional
    public void createTireImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tireImageRepository.findAll().size();

        // Create the TireImage with an existing ID
        TireImage existingTireImage = new TireImage();
        existingTireImage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTireImageMockMvc.perform(post("/api/tire-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingTireImage)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<TireImage> tireImageList = tireImageRepository.findAll();
        assertThat(tireImageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTireImages() throws Exception {
        // Initialize the database
        tireImageRepository.saveAndFlush(tireImage);

        // Get all the tireImageList
        restTireImageMockMvc.perform(get("/api/tire-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tireImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].imagePath").value(hasItem(DEFAULT_IMAGE_PATH.toString())));
    }

    @Test
    @Transactional
    public void getTireImage() throws Exception {
        // Initialize the database
        tireImageRepository.saveAndFlush(tireImage);

        // Get the tireImage
        restTireImageMockMvc.perform(get("/api/tire-images/{id}", tireImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tireImage.getId().intValue()))
            .andExpect(jsonPath("$.imagePath").value(DEFAULT_IMAGE_PATH.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTireImage() throws Exception {
        // Get the tireImage
        restTireImageMockMvc.perform(get("/api/tire-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTireImage() throws Exception {
        // Initialize the database
        tireImageRepository.saveAndFlush(tireImage);
        tireImageSearchRepository.save(tireImage);
        int databaseSizeBeforeUpdate = tireImageRepository.findAll().size();

        // Update the tireImage
        TireImage updatedTireImage = tireImageRepository.findOne(tireImage.getId());
        updatedTireImage
                .imagePath(UPDATED_IMAGE_PATH);

        restTireImageMockMvc.perform(put("/api/tire-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTireImage)))
            .andExpect(status().isOk());

        // Validate the TireImage in the database
        List<TireImage> tireImageList = tireImageRepository.findAll();
        assertThat(tireImageList).hasSize(databaseSizeBeforeUpdate);
        TireImage testTireImage = tireImageList.get(tireImageList.size() - 1);
        assertThat(testTireImage.getImagePath()).isEqualTo(UPDATED_IMAGE_PATH);

        // Validate the TireImage in ElasticSearch
        TireImage tireImageEs = tireImageSearchRepository.findOne(testTireImage.getId());
        assertThat(tireImageEs).isEqualToComparingFieldByField(testTireImage);
    }

    @Test
    @Transactional
    public void updateNonExistingTireImage() throws Exception {
        int databaseSizeBeforeUpdate = tireImageRepository.findAll().size();

        // Create the TireImage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTireImageMockMvc.perform(put("/api/tire-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tireImage)))
            .andExpect(status().isCreated());

        // Validate the TireImage in the database
        List<TireImage> tireImageList = tireImageRepository.findAll();
        assertThat(tireImageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTireImage() throws Exception {
        // Initialize the database
        tireImageRepository.saveAndFlush(tireImage);
        tireImageSearchRepository.save(tireImage);
        int databaseSizeBeforeDelete = tireImageRepository.findAll().size();

        // Get the tireImage
        restTireImageMockMvc.perform(delete("/api/tire-images/{id}", tireImage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean tireImageExistsInEs = tireImageSearchRepository.exists(tireImage.getId());
        assertThat(tireImageExistsInEs).isFalse();

        // Validate the database is empty
        List<TireImage> tireImageList = tireImageRepository.findAll();
        assertThat(tireImageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTireImage() throws Exception {
        // Initialize the database
        tireImageRepository.saveAndFlush(tireImage);
        tireImageSearchRepository.save(tireImage);

        // Search the tireImage
        restTireImageMockMvc.perform(get("/api/_search/tire-images?query=id:" + tireImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tireImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].imagePath").value(hasItem(DEFAULT_IMAGE_PATH.toString())));
    }
}
