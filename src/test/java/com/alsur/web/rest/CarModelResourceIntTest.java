package com.alsur.web.rest;

import com.alsur.RosmiApp;

import com.alsur.domain.CarModel;
import com.alsur.repository.CarModelRepository;
import com.alsur.repository.search.CarModelSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarModelResource REST controller.
 *
 * @see CarModelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RosmiApp.class)
public class CarModelResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Inject
    private CarModelRepository carModelRepository;

    @Inject
    private CarModelSearchRepository carModelSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restCarModelMockMvc;

    private CarModel carModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CarModelResource carModelResource = new CarModelResource();
        ReflectionTestUtils.setField(carModelResource, "carModelSearchRepository", carModelSearchRepository);
        ReflectionTestUtils.setField(carModelResource, "carModelRepository", carModelRepository);
        this.restCarModelMockMvc = MockMvcBuilders.standaloneSetup(carModelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarModel createEntity(EntityManager em) {
        CarModel carModel = new CarModel()
                .name(DEFAULT_NAME)
                .code(DEFAULT_CODE);
        return carModel;
    }

    @Before
    public void initTest() {
        carModelSearchRepository.deleteAll();
        carModel = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarModel() throws Exception {
        int databaseSizeBeforeCreate = carModelRepository.findAll().size();

        // Create the CarModel

        restCarModelMockMvc.perform(post("/api/car-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carModel)))
            .andExpect(status().isCreated());

        // Validate the CarModel in the database
        List<CarModel> carModelList = carModelRepository.findAll();
        assertThat(carModelList).hasSize(databaseSizeBeforeCreate + 1);
        CarModel testCarModel = carModelList.get(carModelList.size() - 1);
        assertThat(testCarModel.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCarModel.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the CarModel in ElasticSearch
        CarModel carModelEs = carModelSearchRepository.findOne(testCarModel.getId());
        assertThat(carModelEs).isEqualToComparingFieldByField(testCarModel);
    }

    @Test
    @Transactional
    public void createCarModelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carModelRepository.findAll().size();

        // Create the CarModel with an existing ID
        CarModel existingCarModel = new CarModel();
        existingCarModel.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarModelMockMvc.perform(post("/api/car-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingCarModel)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CarModel> carModelList = carModelRepository.findAll();
        assertThat(carModelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = carModelRepository.findAll().size();
        // set the field null
        carModel.setName(null);

        // Create the CarModel, which fails.

        restCarModelMockMvc.perform(post("/api/car-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carModel)))
            .andExpect(status().isBadRequest());

        List<CarModel> carModelList = carModelRepository.findAll();
        assertThat(carModelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCarModels() throws Exception {
        // Initialize the database
        carModelRepository.saveAndFlush(carModel);

        // Get all the carModelList
        restCarModelMockMvc.perform(get("/api/car-models?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carModel.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getCarModel() throws Exception {
        // Initialize the database
        carModelRepository.saveAndFlush(carModel);

        // Get the carModel
        restCarModelMockMvc.perform(get("/api/car-models/{id}", carModel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carModel.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCarModel() throws Exception {
        // Get the carModel
        restCarModelMockMvc.perform(get("/api/car-models/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarModel() throws Exception {
        // Initialize the database
        carModelRepository.saveAndFlush(carModel);
        carModelSearchRepository.save(carModel);
        int databaseSizeBeforeUpdate = carModelRepository.findAll().size();

        // Update the carModel
        CarModel updatedCarModel = carModelRepository.findOne(carModel.getId());
        updatedCarModel
                .name(UPDATED_NAME)
                .code(UPDATED_CODE);

        restCarModelMockMvc.perform(put("/api/car-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCarModel)))
            .andExpect(status().isOk());

        // Validate the CarModel in the database
        List<CarModel> carModelList = carModelRepository.findAll();
        assertThat(carModelList).hasSize(databaseSizeBeforeUpdate);
        CarModel testCarModel = carModelList.get(carModelList.size() - 1);
        assertThat(testCarModel.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCarModel.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the CarModel in ElasticSearch
        CarModel carModelEs = carModelSearchRepository.findOne(testCarModel.getId());
        assertThat(carModelEs).isEqualToComparingFieldByField(testCarModel);
    }

    @Test
    @Transactional
    public void updateNonExistingCarModel() throws Exception {
        int databaseSizeBeforeUpdate = carModelRepository.findAll().size();

        // Create the CarModel

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCarModelMockMvc.perform(put("/api/car-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carModel)))
            .andExpect(status().isCreated());

        // Validate the CarModel in the database
        List<CarModel> carModelList = carModelRepository.findAll();
        assertThat(carModelList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCarModel() throws Exception {
        // Initialize the database
        carModelRepository.saveAndFlush(carModel);
        carModelSearchRepository.save(carModel);
        int databaseSizeBeforeDelete = carModelRepository.findAll().size();

        // Get the carModel
        restCarModelMockMvc.perform(delete("/api/car-models/{id}", carModel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean carModelExistsInEs = carModelSearchRepository.exists(carModel.getId());
        assertThat(carModelExistsInEs).isFalse();

        // Validate the database is empty
        List<CarModel> carModelList = carModelRepository.findAll();
        assertThat(carModelList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCarModel() throws Exception {
        // Initialize the database
        carModelRepository.saveAndFlush(carModel);
        carModelSearchRepository.save(carModel);

        // Search the carModel
        restCarModelMockMvc.perform(get("/api/_search/car-models?query=id:" + carModel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carModel.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }
}
