//package com.alsur.web.rest;
//
//import com.alsur.RosmiApp;
//
//import com.alsur.domain.TireSupplier;
//import com.alsur.repository.TireSupplierRepository;
//import com.alsur.repository.search.TireSupplierSearchRepository;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.util.ReflectionTestUtils;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//
//import javax.inject.Inject;
//import javax.persistence.EntityManager;
//import java.math.BigDecimal;
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
///**
// * Test class for the TireSupplierResource REST controller.
// *
// * @see TireSupplierResource
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = RosmiApp.class)
//public class TireSupplierResourceIntTest {
//
//    private static final BigDecimal DEFAULT_PUBLIC_PRICE = new BigDecimal(0);
//    private static final BigDecimal UPDATED_PUBLIC_PRICE = new BigDecimal(1);
//
//    private static final BigDecimal DEFAULT_INSURANCE_PRICE = new BigDecimal(0);
//    private static final BigDecimal UPDATED_INSURANCE_PRICE = new BigDecimal(1);
//
//    @Inject
//    private TireSupplierRepository tireSupplierRepository;
//
//    @Inject
//    private TireSupplierSearchRepository tireSupplierSearchRepository;
//
//    @Inject
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Inject
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Inject
//    private EntityManager em;
//
//    private MockMvc restTireSupplierMockMvc;
//
//    private TireSupplier tireSupplier;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        TireSupplierResource tireSupplierResource = new TireSupplierResource();
//        ReflectionTestUtils.setField(tireSupplierResource, "tireSupplierSearchRepository", tireSupplierSearchRepository);
//        ReflectionTestUtils.setField(tireSupplierResource, "tireSupplierRepository", tireSupplierRepository);
//        this.restTireSupplierMockMvc = MockMvcBuilders.standaloneSetup(tireSupplierResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setMessageConverters(jacksonMessageConverter).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static TireSupplier createEntity(EntityManager em) {
//        TireSupplier tireSupplier = new TireSupplier()
//                .publicPrice(DEFAULT_PUBLIC_PRICE)
//                .insurancePrice(DEFAULT_INSURANCE_PRICE);
//        return tireSupplier;
//    }
//
//    @Before
//    public void initTest() {
//        tireSupplierSearchRepository.deleteAll();
//        tireSupplier = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createTireSupplier() throws Exception {
//        int databaseSizeBeforeCreate = tireSupplierRepository.findAll().size();
//
//        // Create the TireSupplier
//
//        restTireSupplierMockMvc.perform(post("/api/tire-suppliers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(tireSupplier)))
//            .andExpect(status().isCreated());
//
//        // Validate the TireSupplier in the database
//        List<TireSupplier> tireSupplierList = tireSupplierRepository.findAll();
//        assertThat(tireSupplierList).hasSize(databaseSizeBeforeCreate + 1);
//        TireSupplier testTireSupplier = tireSupplierList.get(tireSupplierList.size() - 1);
//        assertThat(testTireSupplier.getPublicPrice()).isEqualTo(DEFAULT_PUBLIC_PRICE);
//        assertThat(testTireSupplier.getInsurancePrice()).isEqualTo(DEFAULT_INSURANCE_PRICE);
//
//        // Validate the TireSupplier in ElasticSearch
//        TireSupplier tireSupplierEs = tireSupplierSearchRepository.findOne(testTireSupplier.getId());
//        assertThat(tireSupplierEs).isEqualToComparingFieldByField(testTireSupplier);
//    }

//    @Test
//    @Transactional
//    public void createTireSupplierWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = tireSupplierRepository.findAll().size();
//
//        // Create the TireSupplier with an existing ID
//        TireSupplier existingTireSupplier = new TireSupplier();
//        existingTireSupplier.setId(1L);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restTireSupplierMockMvc.perform(post("/api/tire-suppliers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(existingTireSupplier)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the Alice in the database
//        List<TireSupplier> tireSupplierList = tireSupplierRepository.findAll();
//        assertThat(tireSupplierList).hasSize(databaseSizeBeforeCreate);
//    }
//
//    @Test
//    @Transactional
//    public void checkPublicPriceIsRequired() throws Exception {
//        int databaseSizeBeforeTest = tireSupplierRepository.findAll().size();
//        // set the field null
//        tireSupplier.setPublicPrice(null);
//
//        // Create the TireSupplier, which fails.
//
//        restTireSupplierMockMvc.perform(post("/api/tire-suppliers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(tireSupplier)))
//            .andExpect(status().isBadRequest());
//
//        List<TireSupplier> tireSupplierList = tireSupplierRepository.findAll();
//        assertThat(tireSupplierList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkInsurancePriceIsRequired() throws Exception {
//        int databaseSizeBeforeTest = tireSupplierRepository.findAll().size();
//        // set the field null
//        tireSupplier.setInsurancePrice(null);
//
//        // Create the TireSupplier, which fails.
//
//        restTireSupplierMockMvc.perform(post("/api/tire-suppliers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(tireSupplier)))
//            .andExpect(status().isBadRequest());
//
//        List<TireSupplier> tireSupplierList = tireSupplierRepository.findAll();
//        assertThat(tireSupplierList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllTireSuppliers() throws Exception {
//        // Initialize the database
//        tireSupplierRepository.saveAndFlush(tireSupplier);
//
//        // Get all the tireSupplierList
//        restTireSupplierMockMvc.perform(get("/api/tire-suppliers?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(tireSupplier.getId().intValue())))
//            .andExpect(jsonPath("$.[*].publicPrice").value(hasItem(DEFAULT_PUBLIC_PRICE.intValue())))
//            .andExpect(jsonPath("$.[*].insurancePrice").value(hasItem(DEFAULT_INSURANCE_PRICE.intValue())));
//    }
//
//    @Test
//    @Transactional
//    public void getTireSupplier() throws Exception {
//        // Initialize the database
//        tireSupplierRepository.saveAndFlush(tireSupplier);
//
//        // Get the tireSupplier
//        restTireSupplierMockMvc.perform(get("/api/tire-suppliers/{id}", tireSupplier.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.id").value(tireSupplier.getId().intValue()))
//            .andExpect(jsonPath("$.publicPrice").value(DEFAULT_PUBLIC_PRICE.intValue()))
//            .andExpect(jsonPath("$.insurancePrice").value(DEFAULT_INSURANCE_PRICE.intValue()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingTireSupplier() throws Exception {
//        // Get the tireSupplier
//        restTireSupplierMockMvc.perform(get("/api/tire-suppliers/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateTireSupplier() throws Exception {
//        // Initialize the database
//        tireSupplierRepository.saveAndFlush(tireSupplier);
//        tireSupplierSearchRepository.save(tireSupplier);
//        int databaseSizeBeforeUpdate = tireSupplierRepository.findAll().size();
//
//        // Update the tireSupplier
//        TireSupplier updatedTireSupplier = tireSupplierRepository.findOne(tireSupplier.getId());
//        updatedTireSupplier
//                .publicPrice(UPDATED_PUBLIC_PRICE)
//                .insurancePrice(UPDATED_INSURANCE_PRICE);
//
//        restTireSupplierMockMvc.perform(put("/api/tire-suppliers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(updatedTireSupplier)))
//            .andExpect(status().isOk());
//
//        // Validate the TireSupplier in the database
//        List<TireSupplier> tireSupplierList = tireSupplierRepository.findAll();
//        assertThat(tireSupplierList).hasSize(databaseSizeBeforeUpdate);
//        TireSupplier testTireSupplier = tireSupplierList.get(tireSupplierList.size() - 1);
//        assertThat(testTireSupplier.getPublicPrice()).isEqualTo(UPDATED_PUBLIC_PRICE);
//        assertThat(testTireSupplier.getInsurancePrice()).isEqualTo(UPDATED_INSURANCE_PRICE);
//
//        // Validate the TireSupplier in ElasticSearch
//        TireSupplier tireSupplierEs = tireSupplierSearchRepository.findOne(testTireSupplier.getId());
//        assertThat(tireSupplierEs).isEqualToComparingFieldByField(testTireSupplier);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingTireSupplier() throws Exception {
//        int databaseSizeBeforeUpdate = tireSupplierRepository.findAll().size();
//
//        // Create the TireSupplier
//
//        // If the entity doesn't have an ID, it will be created instead of just being updated
//        restTireSupplierMockMvc.perform(put("/api/tire-suppliers")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(tireSupplier)))
//            .andExpect(status().isCreated());
//
//        // Validate the TireSupplier in the database
//        List<TireSupplier> tireSupplierList = tireSupplierRepository.findAll();
//        assertThat(tireSupplierList).hasSize(databaseSizeBeforeUpdate + 1);
//    }
//
//    @Test
//    @Transactional
//    public void deleteTireSupplier() throws Exception {
//        // Initialize the database
//        tireSupplierRepository.saveAndFlush(tireSupplier);
//        tireSupplierSearchRepository.save(tireSupplier);
//        int databaseSizeBeforeDelete = tireSupplierRepository.findAll().size();
//
//        // Get the tireSupplier
//        restTireSupplierMockMvc.perform(delete("/api/tire-suppliers/{id}", tireSupplier.getId())
//            .accept(TestUtil.APPLICATION_JSON_UTF8))
//            .andExpect(status().isOk());
//
//        // Validate ElasticSearch is empty
//        boolean tireSupplierExistsInEs = tireSupplierSearchRepository.exists(tireSupplier.getId());
//        assertThat(tireSupplierExistsInEs).isFalse();
//
//        // Validate the database is empty
//        List<TireSupplier> tireSupplierList = tireSupplierRepository.findAll();
//        assertThat(tireSupplierList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//
//    @Test
//    @Transactional
//    public void searchTireSupplier() throws Exception {
//        // Initialize the database
//        tireSupplierRepository.saveAndFlush(tireSupplier);
//        tireSupplierSearchRepository.save(tireSupplier);
//
//        // Search the tireSupplier
//        restTireSupplierMockMvc.perform(get("/api/_search/tire-suppliers?query=id:" + tireSupplier.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(tireSupplier.getId().intValue())))
//            .andExpect(jsonPath("$.[*].publicPrice").value(hasItem(DEFAULT_PUBLIC_PRICE.intValue())))
//            .andExpect(jsonPath("$.[*].insurancePrice").value(hasItem(DEFAULT_INSURANCE_PRICE.intValue())));
//    }
//}
