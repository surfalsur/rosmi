package com.alsur.web.rest;

import com.alsur.RosmiApp;

import com.alsur.domain.CarBrand;
import com.alsur.repository.CarBrandRepository;
import com.alsur.repository.search.CarBrandSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarBrandResource REST controller.
 *
 * @see CarBrandResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RosmiApp.class)
public class CarBrandResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Inject
    private CarBrandRepository carBrandRepository;

    @Inject
    private CarBrandSearchRepository carBrandSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restCarBrandMockMvc;

    private CarBrand carBrand;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CarBrandResource carBrandResource = new CarBrandResource();
        ReflectionTestUtils.setField(carBrandResource, "carBrandSearchRepository", carBrandSearchRepository);
        ReflectionTestUtils.setField(carBrandResource, "carBrandRepository", carBrandRepository);
        this.restCarBrandMockMvc = MockMvcBuilders.standaloneSetup(carBrandResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarBrand createEntity(EntityManager em) {
        CarBrand carBrand = new CarBrand()
                .name(DEFAULT_NAME)
                .code(DEFAULT_CODE);
        return carBrand;
    }

    @Before
    public void initTest() {
        carBrandSearchRepository.deleteAll();
        carBrand = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarBrand() throws Exception {
        int databaseSizeBeforeCreate = carBrandRepository.findAll().size();

        // Create the CarBrand

        restCarBrandMockMvc.perform(post("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carBrand)))
            .andExpect(status().isCreated());

        // Validate the CarBrand in the database
        List<CarBrand> carBrandList = carBrandRepository.findAll();
        assertThat(carBrandList).hasSize(databaseSizeBeforeCreate + 1);
        CarBrand testCarBrand = carBrandList.get(carBrandList.size() - 1);
        assertThat(testCarBrand.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCarBrand.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the CarBrand in ElasticSearch
        CarBrand carBrandEs = carBrandSearchRepository.findOne(testCarBrand.getId());
        assertThat(carBrandEs).isEqualToComparingFieldByField(testCarBrand);
    }

    @Test
    @Transactional
    public void createCarBrandWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carBrandRepository.findAll().size();

        // Create the CarBrand with an existing ID
        CarBrand existingCarBrand = new CarBrand();
        existingCarBrand.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarBrandMockMvc.perform(post("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingCarBrand)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CarBrand> carBrandList = carBrandRepository.findAll();
        assertThat(carBrandList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCarBrands() throws Exception {
        // Initialize the database
        carBrandRepository.saveAndFlush(carBrand);

        // Get all the carBrandList
        restCarBrandMockMvc.perform(get("/api/car-brands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carBrand.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getCarBrand() throws Exception {
        // Initialize the database
        carBrandRepository.saveAndFlush(carBrand);

        // Get the carBrand
        restCarBrandMockMvc.perform(get("/api/car-brands/{id}", carBrand.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carBrand.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCarBrand() throws Exception {
        // Get the carBrand
        restCarBrandMockMvc.perform(get("/api/car-brands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarBrand() throws Exception {
        // Initialize the database
        carBrandRepository.saveAndFlush(carBrand);
        carBrandSearchRepository.save(carBrand);
        int databaseSizeBeforeUpdate = carBrandRepository.findAll().size();

        // Update the carBrand
        CarBrand updatedCarBrand = carBrandRepository.findOne(carBrand.getId());
        updatedCarBrand
                .name(UPDATED_NAME)
                .code(UPDATED_CODE);

        restCarBrandMockMvc.perform(put("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCarBrand)))
            .andExpect(status().isOk());

        // Validate the CarBrand in the database
        List<CarBrand> carBrandList = carBrandRepository.findAll();
        assertThat(carBrandList).hasSize(databaseSizeBeforeUpdate);
        CarBrand testCarBrand = carBrandList.get(carBrandList.size() - 1);
        assertThat(testCarBrand.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCarBrand.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the CarBrand in ElasticSearch
        CarBrand carBrandEs = carBrandSearchRepository.findOne(testCarBrand.getId());
        assertThat(carBrandEs).isEqualToComparingFieldByField(testCarBrand);
    }

    @Test
    @Transactional
    public void updateNonExistingCarBrand() throws Exception {
        int databaseSizeBeforeUpdate = carBrandRepository.findAll().size();

        // Create the CarBrand

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCarBrandMockMvc.perform(put("/api/car-brands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(carBrand)))
            .andExpect(status().isCreated());

        // Validate the CarBrand in the database
        List<CarBrand> carBrandList = carBrandRepository.findAll();
        assertThat(carBrandList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCarBrand() throws Exception {
        // Initialize the database
        carBrandRepository.saveAndFlush(carBrand);
        carBrandSearchRepository.save(carBrand);
        int databaseSizeBeforeDelete = carBrandRepository.findAll().size();

        // Get the carBrand
        restCarBrandMockMvc.perform(delete("/api/car-brands/{id}", carBrand.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean carBrandExistsInEs = carBrandSearchRepository.exists(carBrand.getId());
        assertThat(carBrandExistsInEs).isFalse();

        // Validate the database is empty
        List<CarBrand> carBrandList = carBrandRepository.findAll();
        assertThat(carBrandList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCarBrand() throws Exception {
        // Initialize the database
        carBrandRepository.saveAndFlush(carBrand);
        carBrandSearchRepository.save(carBrand);

        // Search the carBrand
        restCarBrandMockMvc.perform(get("/api/_search/car-brands?query=id:" + carBrand.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carBrand.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }
}
