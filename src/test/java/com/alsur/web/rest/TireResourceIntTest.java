package com.alsur.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.alsur.RosmiApp;
import com.alsur.domain.Tire;
import com.alsur.repository.TireImageRepository;
import com.alsur.repository.TireRepository;
import com.alsur.repository.search.TireSearchRepository;
import com.alsur.service.TireService;

/**
 * Test class for the TireResource REST controller.
 *
 * @see TireResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RosmiApp.class)
public class TireResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SIZE = "AAAAAAAAAA";
    private static final String UPDATED_SIZE = "BBBBBBBBBB";

    @Inject
    private TireRepository tireRepository;

    @Inject
    private TireService tireService;

    @Inject
    private TireSearchRepository tireSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restTireMockMvc;

    private Tire tire;

    @Inject
    private TireImageRepository tireImageRepository;
     
    
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TireResource tireResource = new TireResource();
        ReflectionTestUtils.setField(tireResource, "tireService", tireService);
        ReflectionTestUtils.setField(tireResource, "tireImageRepository", tireImageRepository);
        this.restTireMockMvc = MockMvcBuilders.standaloneSetup(tireResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tire createEntity(EntityManager em) {
        Tire tire = new Tire()
                .name(DEFAULT_NAME)
                .size(DEFAULT_SIZE);
        return tire;
    }

    @Before
    public void initTest() {
        tireSearchRepository.deleteAll();
        tire = createEntity(em);
    }

    @Test
    @Transactional
    public void createTire() throws Exception {
        int databaseSizeBeforeCreate = tireRepository.findAll().size();

        // Create the Tire

        restTireMockMvc.perform(post("/api/tires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tire)))
            .andExpect(status().isCreated());

        // Validate the Tire in the database
        List<Tire> tireList = tireRepository.findAll();
        assertThat(tireList).hasSize(databaseSizeBeforeCreate + 1);
        Tire testTire = tireList.get(tireList.size() - 1);
        assertThat(testTire.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTire.getSize()).isEqualTo(DEFAULT_SIZE);

        // Validate the Tire in ElasticSearch
        Tire tireEs = tireSearchRepository.findOne(testTire.getId());
        assertThat(tireEs).isEqualToComparingFieldByField(testTire);
    }

    @Test
    @Transactional
    public void createTireWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tireRepository.findAll().size();

        // Create the Tire with an existing ID
        Tire existingTire = new Tire();
        existingTire.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTireMockMvc.perform(post("/api/tires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingTire)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Tire> tireList = tireRepository.findAll();
        assertThat(tireList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTires() throws Exception {
        // Initialize the database
        tireRepository.saveAndFlush(tire);

        // Get all the tireList
        restTireMockMvc.perform(get("/api/tires?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tire.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE.toString())));
    }

    @Test
    @Transactional
    public void getTire() throws Exception {
        // Initialize the database
        tireRepository.saveAndFlush(tire);

        // Get the tire
        restTireMockMvc.perform(get("/api/tires/{id}", tire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tire.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.size").value(DEFAULT_SIZE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTire() throws Exception {
        // Get the tire
        restTireMockMvc.perform(get("/api/tires/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTire() throws Exception {
        // Initialize the database
        tireService.save(tire);

        int databaseSizeBeforeUpdate = tireRepository.findAll().size();

        // Update the tire
        Tire updatedTire = tireRepository.findOne(tire.getId());
        updatedTire
                .name(UPDATED_NAME)
                .size(UPDATED_SIZE);

        restTireMockMvc.perform(put("/api/tires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTire)))
            .andExpect(status().isOk());

        // Validate the Tire in the database
        List<Tire> tireList = tireRepository.findAll();
        assertThat(tireList).hasSize(databaseSizeBeforeUpdate);
        Tire testTire = tireList.get(tireList.size() - 1);
        assertThat(testTire.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTire.getSize()).isEqualTo(UPDATED_SIZE);

        // Validate the Tire in ElasticSearch
        Tire tireEs = tireSearchRepository.findOne(testTire.getId());
        assertThat(tireEs).isEqualToComparingFieldByField(testTire);
    }

    @Test
    @Transactional
    public void updateNonExistingTire() throws Exception {
        int databaseSizeBeforeUpdate = tireRepository.findAll().size();

        // Create the Tire

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTireMockMvc.perform(put("/api/tires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tire)))
            .andExpect(status().isCreated());

        // Validate the Tire in the database
        List<Tire> tireList = tireRepository.findAll();
        assertThat(tireList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTire() throws Exception {
        // Initialize the database
        tireService.save(tire);

        int databaseSizeBeforeDelete = tireRepository.findAll().size();

        // Get the tire
        restTireMockMvc.perform(delete("/api/tires/{id}", tire.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean tireExistsInEs = tireSearchRepository.exists(tire.getId());
        assertThat(tireExistsInEs).isFalse();

        // Validate the database is empty
        List<Tire> tireList = tireRepository.findAll();
        assertThat(tireList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTire() throws Exception {
        // Initialize the database
        tireService.save(tire);

        // Search the tire
        restTireMockMvc.perform(get("/api/_search/tires?query=id:" + tire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tire.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE.toString())));
    }
    
    
    
    @Test
    @Transactional
    public void shouldGetAllTiresWithParams() throws Exception {
        // Initialize the database
        tireRepository.saveAndFlush(tire);

        // Get all the tireList
        restTireMockMvc.perform(get("/api/tires").param("branchOfficeId", "")
        		.param("carModelId", "")
        		.param("supplierId", "")
        		.param("tireBrandId", "")
        		.param("tireName", ""))
            .andExpect(status().isOk());
    }
    
    
    @Test
    @Transactional
    public void createTireWithImages() throws Exception {
        	
    	MockMultipartFile firstFile = new MockMultipartFile("image", "filename.png", "image/png", "some image".getBytes());
    	MockMultipartFile secondFile = new MockMultipartFile("image", "other-file-name.png", "image/png", "some other file".getBytes());
        MockMultipartFile jsonFile = new MockMultipartFile("tire", "", "application/json", TestUtil.convertObjectToJsonBytes(tire));
    
           
         // Create the Tire
            restTireMockMvc.perform(fileUpload("/api/tiresWithImages")
            		.file(jsonFile).file(firstFile).file(secondFile));
    
            String imagePath = "tire_images/tire_image_1.png";
            
            
            
            assertThat(!tireImageRepository.findByImagePath(imagePath).isEmpty());
            
    }
    
    
}
