package com.alsur.web.rest;

import com.alsur.RosmiApp;

import com.alsur.domain.BranchOffice;
import com.alsur.repository.BranchOfficeRepository;
import com.alsur.repository.search.BranchOfficeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BranchOfficeResource REST controller.
 *
 * @see BranchOfficeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RosmiApp.class)
public class BranchOfficeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "joaquincabal@gmail.com";
    private static final String UPDATED_EMAIL = "joaquincabal@mail.com";

    @Inject
    private BranchOfficeRepository branchOfficeRepository;

    @Inject
    private BranchOfficeSearchRepository branchOfficeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restBranchOfficeMockMvc;

    private BranchOffice branchOffice;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BranchOfficeResource branchOfficeResource = new BranchOfficeResource();
        ReflectionTestUtils.setField(branchOfficeResource, "branchOfficeSearchRepository", branchOfficeSearchRepository);
        ReflectionTestUtils.setField(branchOfficeResource, "branchOfficeRepository", branchOfficeRepository);
        this.restBranchOfficeMockMvc = MockMvcBuilders.standaloneSetup(branchOfficeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BranchOffice createEntity(EntityManager em) {
        BranchOffice branchOffice = new BranchOffice()
                .name(DEFAULT_NAME)
                .address(DEFAULT_ADDRESS)
                .phone(DEFAULT_PHONE)
                .email(DEFAULT_EMAIL);
        return branchOffice;
    }

    @Before
    public void initTest() {
        branchOfficeSearchRepository.deleteAll();
        branchOffice = createEntity(em);
    }

    @Test
    @Transactional
    public void createBranchOffice() throws Exception {
        int databaseSizeBeforeCreate = branchOfficeRepository.findAll().size();

        // Create the BranchOffice

        restBranchOfficeMockMvc.perform(post("/api/branch-offices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(branchOffice)))
            .andExpect(status().isCreated());

        // Validate the BranchOffice in the database
        List<BranchOffice> branchOfficeList = branchOfficeRepository.findAll();
        assertThat(branchOfficeList).hasSize(databaseSizeBeforeCreate + 1);
        BranchOffice testBranchOffice = branchOfficeList.get(branchOfficeList.size() - 1);
        assertThat(testBranchOffice.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBranchOffice.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testBranchOffice.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testBranchOffice.getEmail()).isEqualTo(DEFAULT_EMAIL);

        // Validate the BranchOffice in ElasticSearch
        BranchOffice branchOfficeEs = branchOfficeSearchRepository.findOne(testBranchOffice.getId());
        assertThat(branchOfficeEs).isEqualToComparingFieldByField(testBranchOffice);
    }

    @Test
    @Transactional
    public void createBranchOfficeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = branchOfficeRepository.findAll().size();

        // Create the BranchOffice with an existing ID
        BranchOffice existingBranchOffice = new BranchOffice();
        existingBranchOffice.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBranchOfficeMockMvc.perform(post("/api/branch-offices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingBranchOffice)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<BranchOffice> branchOfficeList = branchOfficeRepository.findAll();
        assertThat(branchOfficeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = branchOfficeRepository.findAll().size();
        // set the field null
        branchOffice.setName(null);

        // Create the BranchOffice, which fails.

        restBranchOfficeMockMvc.perform(post("/api/branch-offices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(branchOffice)))
            .andExpect(status().isBadRequest());

        List<BranchOffice> branchOfficeList = branchOfficeRepository.findAll();
        assertThat(branchOfficeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBranchOffices() throws Exception {
        // Initialize the database
        branchOfficeRepository.saveAndFlush(branchOffice);

        // Get all the branchOfficeList
        restBranchOfficeMockMvc.perform(get("/api/branch-offices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(branchOffice.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())));
    }

    @Test
    @Transactional
    public void getBranchOffice() throws Exception {
        // Initialize the database
        branchOfficeRepository.saveAndFlush(branchOffice);

        // Get the branchOffice
        restBranchOfficeMockMvc.perform(get("/api/branch-offices/{id}", branchOffice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(branchOffice.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBranchOffice() throws Exception {
        // Get the branchOffice
        restBranchOfficeMockMvc.perform(get("/api/branch-offices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBranchOffice() throws Exception {
        // Initialize the database
        branchOfficeRepository.saveAndFlush(branchOffice);
        branchOfficeSearchRepository.save(branchOffice);
        int databaseSizeBeforeUpdate = branchOfficeRepository.findAll().size();

        // Update the branchOffice
        BranchOffice updatedBranchOffice = branchOfficeRepository.findOne(branchOffice.getId());
        updatedBranchOffice
                .name(UPDATED_NAME)
                .address(UPDATED_ADDRESS)
                .phone(UPDATED_PHONE)
                .email(UPDATED_EMAIL);

        restBranchOfficeMockMvc.perform(put("/api/branch-offices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBranchOffice)))
            .andExpect(status().isOk());

        // Validate the BranchOffice in the database
        List<BranchOffice> branchOfficeList = branchOfficeRepository.findAll();
        assertThat(branchOfficeList).hasSize(databaseSizeBeforeUpdate);
        BranchOffice testBranchOffice = branchOfficeList.get(branchOfficeList.size() - 1);
        assertThat(testBranchOffice.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testBranchOffice.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testBranchOffice.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testBranchOffice.getEmail()).isEqualTo(UPDATED_EMAIL);

        // Validate the BranchOffice in ElasticSearch
        BranchOffice branchOfficeEs = branchOfficeSearchRepository.findOne(testBranchOffice.getId());
        assertThat(branchOfficeEs).isEqualToComparingFieldByField(testBranchOffice);
    }

    @Test
    @Transactional
    public void updateNonExistingBranchOffice() throws Exception {
        int databaseSizeBeforeUpdate = branchOfficeRepository.findAll().size();

        // Create the BranchOffice

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBranchOfficeMockMvc.perform(put("/api/branch-offices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(branchOffice)))
            .andExpect(status().isCreated());

        // Validate the BranchOffice in the database
        List<BranchOffice> branchOfficeList = branchOfficeRepository.findAll();
        assertThat(branchOfficeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBranchOffice() throws Exception {
        // Initialize the database
        branchOfficeRepository.saveAndFlush(branchOffice);
        branchOfficeSearchRepository.save(branchOffice);
        int databaseSizeBeforeDelete = branchOfficeRepository.findAll().size();

        // Get the branchOffice
        restBranchOfficeMockMvc.perform(delete("/api/branch-offices/{id}", branchOffice.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean branchOfficeExistsInEs = branchOfficeSearchRepository.exists(branchOffice.getId());
        assertThat(branchOfficeExistsInEs).isFalse();

        // Validate the database is empty
        List<BranchOffice> branchOfficeList = branchOfficeRepository.findAll();
        assertThat(branchOfficeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchBranchOffice() throws Exception {
        // Initialize the database
        branchOfficeRepository.saveAndFlush(branchOffice);
        branchOfficeSearchRepository.save(branchOffice);

        // Search the branchOffice
        restBranchOfficeMockMvc.perform(get("/api/_search/branch-offices?query=id:" + branchOffice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(branchOffice.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())));
    }
}
