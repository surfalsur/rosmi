package com.alsur.repository;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.alsur.RosmiApp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RosmiApp.class)
@Transactional
public class TireRepositoryTest {
	
	@Inject
	TireRepository tireRepository;
	
	
	@Before
	public void setUp() {
		 
	 
	}
	
	@Test
	public void shouldFindTiresByParams() {
		
		String tireName = null;
		Long tireBrandId = null;
		Long supplierId = null;
		Long branchOfficeId = null;
		Long carModelId = null;
		
		PageRequest pageable = new PageRequest(
				  0, 20, Direction.ASC, "name");
		
		tireRepository.findAllWithParams(branchOfficeId,
				carModelId,
				supplierId,
				tireBrandId,
				tireName,
				pageable);
		
	}
} 
