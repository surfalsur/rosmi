package com.alsur.repository;

import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.alsur.RosmiApp;
import com.alsur.domain.TireImage;
import static org.assertj.core.api.Assertions.assertThat;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RosmiApp.class)
@Transactional
public class TireImageRepositoryTest {
	
	@Inject
	TireImageRepository tireImageRepository;
	
	
	@Before
	public void setUp() {
		 
	 
	}
	
	@Test
	public void shouldFindTireImages() {
		
		List<TireImage> tireImages = tireImageRepository.findByTireId(1l);
		
		assertThat(!tireImages.isEmpty());
		
	}
} 
