'use strict';

describe('Controller Tests', function() {

    describe('Tire Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockTire, MockBranchOffice, MockCarModel, MockTireBrand;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockTire = jasmine.createSpy('MockTire');
            MockBranchOffice = jasmine.createSpy('MockBranchOffice');
            MockCarModel = jasmine.createSpy('MockCarModel');
            MockTireBrand = jasmine.createSpy('MockTireBrand');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Tire': MockTire,
                'BranchOffice': MockBranchOffice,
                'CarModel': MockCarModel,
                'TireBrand': MockTireBrand
            };
            createController = function() {
                $injector.get('$controller')("TireDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'rosmiApp:tireUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
