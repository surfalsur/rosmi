'use strict';

describe('Controller Tests', function() {

    describe('TireSupplier Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockTireSupplier, MockSupplier, MockTire;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockTireSupplier = jasmine.createSpy('MockTireSupplier');
            MockSupplier = jasmine.createSpy('MockSupplier');
            MockTire = jasmine.createSpy('MockTire');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'TireSupplier': MockTireSupplier,
                'Supplier': MockSupplier,
                'Tire': MockTire
            };
            createController = function() {
                $injector.get('$controller')("TireSupplierDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'rosmiApp:tireSupplierUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
