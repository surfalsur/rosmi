(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .constant('paginationConstants', {
            'itemsPerPage': 8
        });
})();
