(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('car-model', {
            parent: 'entity',
            url: '/car-model?page&sort&search',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.carModel.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/car-model/car-models.html',
                    controller: 'CarModelController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('carModel');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('car-model-detail', {
            parent: 'entity',
            url: '/car-model/{id}',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.carModel.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/car-model/car-model-detail.html',
                    controller: 'CarModelDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('carModel');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CarModel', function($stateParams, CarModel) {
                    return CarModel.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'car-model',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('car-model-detail.edit', {
            parent: 'car-model-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/car-model/car-model-dialog.html',
                    controller: 'CarModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CarModel', function(CarModel) {
                            return CarModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('car-model.new', {
            parent: 'car-model',
            url: '/new',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/car-model/car-model-dialog.html',
                    controller: 'CarModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                code: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('car-model', null, { reload: 'car-model' });
                }, function() {
                    $state.go('car-model');
                });
            }]
        })
        .state('car-model.edit', {
            parent: 'car-model',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/car-model/car-model-dialog.html',
                    controller: 'CarModelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CarModel', function(CarModel) {
                            return CarModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('car-model', null, { reload: 'car-model' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('car-model.delete', {
            parent: 'car-model',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/car-model/car-model-delete-dialog.html',
                    controller: 'CarModelDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CarModel', function(CarModel) {
                            return CarModel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('car-model', null, { reload: 'car-model' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
