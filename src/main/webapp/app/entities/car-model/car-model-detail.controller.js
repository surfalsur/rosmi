(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('CarModelDetailController', CarModelDetailController);

    CarModelDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'CarModel', 'CarBrand'];

    function CarModelDetailController($scope, $rootScope, $stateParams, previousState, entity, CarModel, CarBrand) {
        var vm = this;

        vm.carModel = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rosmiApp:carModelUpdate', function(event, result) {
            vm.carModel = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
