(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireBrandDialogController', TireBrandDialogController);

    TireBrandDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TireBrand'];

    function TireBrandDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TireBrand) {
        var vm = this;

        vm.tireBrand = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tireBrand.id !== null) {
                TireBrand.update(vm.tireBrand, onSaveSuccess, onSaveError);
            } else {
                TireBrand.save(vm.tireBrand, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('rosmiApp:tireBrandUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
