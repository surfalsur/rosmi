(function() {
    'use strict';
    angular
        .module('rosmiApp')
        .factory('TireBrand', TireBrand);

    TireBrand.$inject = ['$resource'];

    function TireBrand ($resource) {
        var resourceUrl =  'api/tire-brands/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
