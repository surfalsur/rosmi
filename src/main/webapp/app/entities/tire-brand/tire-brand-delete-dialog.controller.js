(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireBrandDeleteController',TireBrandDeleteController);

    TireBrandDeleteController.$inject = ['$uibModalInstance', 'entity', 'TireBrand'];

    function TireBrandDeleteController($uibModalInstance, entity, TireBrand) {
        var vm = this;

        vm.tireBrand = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TireBrand.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
