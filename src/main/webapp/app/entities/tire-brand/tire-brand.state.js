(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('tire-brand', {
            parent: 'entity',
            url: '/tire-brand?page&sort&search',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.tireBrand.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tire-brand/tire-brands.html',
                    controller: 'TireBrandController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tireBrand');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('tire-brand-detail', {
            parent: 'entity',
            url: '/tire-brand/{id}',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.tireBrand.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tire-brand/tire-brand-detail.html',
                    controller: 'TireBrandDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tireBrand');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TireBrand', function($stateParams, TireBrand) {
                    return TireBrand.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'tire-brand',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('tire-brand-detail.edit', {
            parent: 'tire-brand-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-brand/tire-brand-dialog.html',
                    controller: 'TireBrandDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TireBrand', function(TireBrand) {
                            return TireBrand.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tire-brand.new', {
            parent: 'tire-brand',
            url: '/new',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-brand/tire-brand-dialog.html',
                    controller: 'TireBrandDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                code: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('tire-brand', null, { reload: 'tire-brand' });
                }, function() {
                    $state.go('tire-brand');
                });
            }]
        })
        .state('tire-brand.edit', {
            parent: 'tire-brand',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-brand/tire-brand-dialog.html',
                    controller: 'TireBrandDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TireBrand', function(TireBrand) {
                            return TireBrand.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tire-brand', null, { reload: 'tire-brand' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tire-brand.delete', {
            parent: 'tire-brand',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-brand/tire-brand-delete-dialog.html',
                    controller: 'TireBrandDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TireBrand', function(TireBrand) {
                            return TireBrand.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tire-brand', null, { reload: 'tire-brand' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
