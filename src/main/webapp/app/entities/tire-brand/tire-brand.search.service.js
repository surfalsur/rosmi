(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .factory('TireBrandSearch', TireBrandSearch);

    TireBrandSearch.$inject = ['$resource'];

    function TireBrandSearch($resource) {
        var resourceUrl =  'api/_search/tire-brands/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
