(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireBrandDetailController', TireBrandDetailController);

    TireBrandDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TireBrand'];

    function TireBrandDetailController($scope, $rootScope, $stateParams, previousState, entity, TireBrand) {
        var vm = this;

        vm.tireBrand = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rosmiApp:tireBrandUpdate', function(event, result) {
            vm.tireBrand = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
