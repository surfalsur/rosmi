(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('CarBrandDeleteController',CarBrandDeleteController);

    CarBrandDeleteController.$inject = ['$uibModalInstance', 'entity', 'CarBrand'];

    function CarBrandDeleteController($uibModalInstance, entity, CarBrand) {
        var vm = this;

        vm.carBrand = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CarBrand.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
