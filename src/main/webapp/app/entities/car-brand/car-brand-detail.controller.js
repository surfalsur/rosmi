(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('CarBrandDetailController', CarBrandDetailController);

    CarBrandDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'CarBrand'];

    function CarBrandDetailController($scope, $rootScope, $stateParams, previousState, entity, CarBrand) {
        var vm = this;

        vm.carBrand = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rosmiApp:carBrandUpdate', function(event, result) {
            vm.carBrand = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
