(function() {
    'use strict';
    angular
        .module('rosmiApp')
        .factory('BranchOffice', BranchOffice);

    BranchOffice.$inject = ['$resource'];

    function BranchOffice ($resource) {
        var resourceUrl =  'api/branch-offices/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
