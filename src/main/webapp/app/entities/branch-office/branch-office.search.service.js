(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .factory('BranchOfficeSearch', BranchOfficeSearch);

    BranchOfficeSearch.$inject = ['$resource'];

    function BranchOfficeSearch($resource) {
        var resourceUrl =  'api/_search/branch-offices/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
