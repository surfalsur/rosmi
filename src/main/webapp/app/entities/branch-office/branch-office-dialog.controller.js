(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('BranchOfficeDialogController', BranchOfficeDialogController);

    BranchOfficeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'BranchOffice'];

    function BranchOfficeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, BranchOffice) {
        var vm = this;

        vm.branchOffice = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.branchOffice.id !== null) {
                BranchOffice.update(vm.branchOffice, onSaveSuccess, onSaveError);
            } else {
                BranchOffice.save(vm.branchOffice, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('rosmiApp:branchOfficeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
