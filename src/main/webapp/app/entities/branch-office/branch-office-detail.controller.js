(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('BranchOfficeDetailController', BranchOfficeDetailController);

    BranchOfficeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'BranchOffice'];

    function BranchOfficeDetailController($scope, $rootScope, $stateParams, previousState, entity, BranchOffice) {
        var vm = this;

        vm.branchOffice = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rosmiApp:branchOfficeUpdate', function(event, result) {
            vm.branchOffice = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
