(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('branch-office', {
            parent: 'entity',
            url: '/branch-office?page&sort&search',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.branchOffice.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/branch-office/branch-offices.html',
                    controller: 'BranchOfficeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('branchOffice');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('branch-office-detail', {
            parent: 'entity',
            url: '/branch-office/{id}',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.branchOffice.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/branch-office/branch-office-detail.html',
                    controller: 'BranchOfficeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('branchOffice');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'BranchOffice', function($stateParams, BranchOffice) {
                    return BranchOffice.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'branch-office',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('branch-office-detail.edit', {
            parent: 'branch-office-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/branch-office/branch-office-dialog.html',
                    controller: 'BranchOfficeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['BranchOffice', function(BranchOffice) {
                            return BranchOffice.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('branch-office.new', {
            parent: 'branch-office',
            url: '/new',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/branch-office/branch-office-dialog.html',
                    controller: 'BranchOfficeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                address: null,
                                phone: null,
                                email: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('branch-office', null, { reload: 'branch-office' });
                }, function() {
                    $state.go('branch-office');
                });
            }]
        })
        .state('branch-office.edit', {
            parent: 'branch-office',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/branch-office/branch-office-dialog.html',
                    controller: 'BranchOfficeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['BranchOffice', function(BranchOffice) {
                            return BranchOffice.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('branch-office', null, { reload: 'branch-office' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('branch-office.delete', {
            parent: 'branch-office',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/branch-office/branch-office-delete-dialog.html',
                    controller: 'BranchOfficeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['BranchOffice', function(BranchOffice) {
                            return BranchOffice.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('branch-office', null, { reload: 'branch-office' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
