(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('BranchOfficeDeleteController',BranchOfficeDeleteController);

    BranchOfficeDeleteController.$inject = ['$uibModalInstance', 'entity', 'BranchOffice'];

    function BranchOfficeDeleteController($uibModalInstance, entity, BranchOffice) {
        var vm = this;

        vm.branchOffice = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            BranchOffice.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
