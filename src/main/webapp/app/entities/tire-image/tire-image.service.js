(function() {
    'use strict';
    angular
        .module('rosmiApp')
        .factory('TireImage', TireImage);

    TireImage.$inject = ['$resource'];

    function TireImage ($resource) {
        var resourceUrl =  'api/tire-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
