(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .factory('TireImageSearch', TireImageSearch);

    TireImageSearch.$inject = ['$resource'];

    function TireImageSearch($resource) {
        var resourceUrl =  'api/_search/tire-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
