(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('tire-image', {
            parent: 'entity',
            url: '/tire-image?page&sort&search',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.tireImage.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tire-image/tire-images.html',
                    controller: 'TireImageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tireImage');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('tire-image-detail', {
            parent: 'entity',
            url: '/tire-image/{id}',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.tireImage.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tire-image/tire-image-detail.html',
                    controller: 'TireImageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tireImage');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TireImage', function($stateParams, TireImage) {
                    return TireImage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'tire-image',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('tire-image-detail.edit', {
            parent: 'tire-image-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-image/tire-image-dialog.html',
                    controller: 'TireImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TireImage', function(TireImage) {
                            return TireImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tire-image.new', {
            parent: 'tire-image',
            url: '/new',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-image/tire-image-dialog.html',
                    controller: 'TireImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                imagePath: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('tire-image', null, { reload: 'tire-image' });
                }, function() {
                    $state.go('tire-image');
                });
            }]
        })
        .state('tire-image.edit', {
            parent: 'tire-image',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-image/tire-image-dialog.html',
                    controller: 'TireImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TireImage', function(TireImage) {
                            return TireImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tire-image', null, { reload: 'tire-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tire-image.delete', {
            parent: 'tire-image',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-image/tire-image-delete-dialog.html',
                    controller: 'TireImageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TireImage', function(TireImage) {
                            return TireImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tire-image', null, { reload: 'tire-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
