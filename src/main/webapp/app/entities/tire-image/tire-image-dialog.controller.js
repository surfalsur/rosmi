(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireImageDialogController', TireImageDialogController);

    TireImageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TireImage', 'Tire'];

    function TireImageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TireImage, Tire) {
        var vm = this;

        vm.tireImage = entity;
        vm.clear = clear;
        vm.save = save;
        vm.tires = Tire.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tireImage.id !== null) {
                TireImage.update(vm.tireImage, onSaveSuccess, onSaveError);
            } else {
                TireImage.save(vm.tireImage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('rosmiApp:tireImageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
