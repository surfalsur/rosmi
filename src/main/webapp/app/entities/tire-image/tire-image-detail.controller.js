(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireImageDetailController', TireImageDetailController);

    TireImageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TireImage', 'Tire'];

    function TireImageDetailController($scope, $rootScope, $stateParams, previousState, entity, TireImage, Tire) {
        var vm = this;

        vm.tireImage = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rosmiApp:tireImageUpdate', function(event, result) {
            vm.tireImage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
