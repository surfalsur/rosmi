(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireImageDeleteController',TireImageDeleteController);

    TireImageDeleteController.$inject = ['$uibModalInstance', 'entity', 'TireImage'];

    function TireImageDeleteController($uibModalInstance, entity, TireImage) {
        var vm = this;

        vm.tireImage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TireImage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
