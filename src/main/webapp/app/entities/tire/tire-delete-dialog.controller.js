(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireDeleteController',TireDeleteController);

    TireDeleteController.$inject = ['$uibModalInstance', 'entity', 'Tire'];

    function TireDeleteController($uibModalInstance, entity, Tire) {
        var vm = this;

        vm.tire = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Tire.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
