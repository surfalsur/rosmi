(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .factory('TireSearch', TireSearch);

    TireSearch.$inject = ['$resource'];

    function TireSearch($resource) {
        var resourceUrl =  'api/_search/tires/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
