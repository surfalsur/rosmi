(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireDetailController', TireDetailController);

    TireDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Tire', 'BranchOffice', 'CarModel', 'TireBrand'];

    function TireDetailController($scope, $rootScope, $stateParams, previousState, entity, Tire, BranchOffice, CarModel, TireBrand) {
        var vm = this;

        vm.tire = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rosmiApp:tireUpdate', function(event, result) {
            vm.tire = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
