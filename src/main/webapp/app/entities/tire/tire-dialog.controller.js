(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireDialogController', TireDialogController);

    TireDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'FileUploader', 'entity', 'Tire', 'BranchOffice', 'CarModel', 'TireBrand'];

    function TireDialogController ($timeout, $scope, $stateParams, $uibModalInstance, FileUploader, entity, Tire, BranchOffice, CarModel, TireBrand) {
        var vm = this;

        vm.tire = entity;
        vm.clear = clear;
        vm.save = save;
        vm.branchoffices = BranchOffice.query();
        vm.carmodels = CarModel.query();
        vm.tirebrands = TireBrand.query();
        vm.files = [];

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            var fd = new FormData();
            
            if (vm.tire.id !== null) {
                Tire.update(vm.tire,vm.files, onSaveSuccess, onSaveError);
            } else {
            	
            	//adding images
            	angular.forEach(vm.files,function(data,key) {
            		 fd.append("files",data);
            	});
            	
            	//adding json object
                fd.append('tire', new Blob([angular.toJson(vm.tire)], { type: "application/json" }));
                
                
                Tire.save({},fd, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('rosmiApp:tireUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        //FileUploader Start >
        var uploader = $scope.uploader = new FileUploader({
            url: 'upload.php'
        });

        // FILTERS
        uploader.filters.push({
            name: 'imageFilter',
            fn: function (item /*{File|FileLikeObject}*/ , options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/ , filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            console.info('onAfterAddingFile', fileItem);
            vm.files.push(fileItem._file);
        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);
        //FileUploader Ends<
    }
})();
