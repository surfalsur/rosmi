(function() {
    'use strict';
    angular
        .module('rosmiApp')
        .factory('Tire', Tire);

    Tire.$inject = ['$resource'];

    function Tire ($resource) {
        var resourceUrl =  'api/tires/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {
            	method:'PUT',
            	transformRequest: angular.identity,
            	headers: {'Content-Type':undefined}
            },
            'save': {
                method: 'POST',
                transformRequest: angular.identity,
                headers: {'Content-Type':undefined}
            },
            'getImage': {
                method: 'GET',
                url: '/image',
            }
        });

    }
})();
