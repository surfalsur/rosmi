(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireController', TireController);

    TireController.$inject = ['$scope', '$state', 'Tire', 'TireSearch', 'ParseLinks', 'AlertService',
    'paginationConstants', 'pagingParams','BranchOffice','Supplier','CarModel','TireBrand'];

    function TireController ($scope, $state, Tire, TireSearch, ParseLinks, AlertService,
       paginationConstants, pagingParams,BranchOffice,Supplier,CarModel,TireBrand) {
        var vm = this;

        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;
        vm.searchQuery = pagingParams.search;
        vm.currentSearch = pagingParams.search;

        vm.branchOffices = BranchOffice.query();
        vm.suppliers = Supplier.query();
        vm.carModels = CarModel.query();
        vm.tireBrands = TireBrand.query();

        vm.filterBranchOffice = "";
        vm.filterSupplier = "";
        vm.filterCarModel = "";
        vm.filterTireBrand = "";

        vm.tireName = "";

        vm.image = "/opt/data/tire/1/llanta1.jpg";


        function getImage() {
            return "";
        }

        loadAll();

        function loadAll () {

            Tire.query({
                    branchOfficeId: getOptionalParam(vm.filterBranchOffice),
                    carModelId: getOptionalParam(vm.filterCarModel),
                    supplierId: getOptionalParam(vm.filterSupplier),
                    tireBrandId: getOptionalParam(vm.filterTireBrand),
                    tireName : vm.tireName,
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: sort()
                }, onSuccess, onError);

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.tires = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
            function getOptionalParam(param) {
              return param != "" ? param.id : "";
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function search(searchQuery) {
            if (!searchQuery){
                return vm.clear();
            }
            vm.links = null;
            vm.page = 1;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.transition();
        }

        function clear() {
          vm.filterBranchOffice = "";
          vm.filterSupplier = "";
          vm.filterCarModel = "";
          vm.filterTireBrand = "";
          vm.tireName = "";
          vm.loadAll();
        }



    }
})();
