(function() {
    'use strict';
    angular
        .module('rosmiApp')
        .factory('TireSupplier', TireSupplier);

    TireSupplier.$inject = ['$resource'];

    function TireSupplier ($resource) {
        var resourceUrl =  'api/tire-suppliers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
