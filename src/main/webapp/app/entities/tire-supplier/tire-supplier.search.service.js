(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .factory('TireSupplierSearch', TireSupplierSearch);

    TireSupplierSearch.$inject = ['$resource'];

    function TireSupplierSearch($resource) {
        var resourceUrl =  'api/_search/tire-suppliers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
