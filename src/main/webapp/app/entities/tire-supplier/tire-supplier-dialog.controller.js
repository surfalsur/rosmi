(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireSupplierDialogController', TireSupplierDialogController);

    TireSupplierDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TireSupplier', 'Supplier', 'Tire'];

    function TireSupplierDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TireSupplier, Supplier, Tire) {
        var vm = this;

        vm.tireSupplier = entity;
        vm.clear = clear;
        vm.save = save;
        vm.suppliers = Supplier.query();
        vm.tires = Tire.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tireSupplier.id !== null) {
                TireSupplier.update(vm.tireSupplier, onSaveSuccess, onSaveError);
            } else {
                TireSupplier.save(vm.tireSupplier, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('rosmiApp:tireSupplierUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
