(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireSupplierDeleteController',TireSupplierDeleteController);

    TireSupplierDeleteController.$inject = ['$uibModalInstance', 'entity', 'TireSupplier'];

    function TireSupplierDeleteController($uibModalInstance, entity, TireSupplier) {
        var vm = this;

        vm.tireSupplier = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TireSupplier.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
