(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('tire-supplier', {
            parent: 'entity',
            url: '/tire-supplier?page&sort&search',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.tireSupplier.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tire-supplier/tire-suppliers.html',
                    controller: 'TireSupplierController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tireSupplier');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('tire-supplier-detail', {
            parent: 'entity',
            url: '/tire-supplier/{id}',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN'],
                pageTitle: 'rosmiApp.tireSupplier.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/tire-supplier/tire-supplier-detail.html',
                    controller: 'TireSupplierDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tireSupplier');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TireSupplier', function($stateParams, TireSupplier) {
                    return TireSupplier.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'tire-supplier',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('tire-supplier-detail.edit', {
            parent: 'tire-supplier-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-supplier/tire-supplier-dialog.html',
                    controller: 'TireSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TireSupplier', function(TireSupplier) {
                            return TireSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tire-supplier.new', {
            parent: 'tire-supplier',
            url: '/new',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-supplier/tire-supplier-dialog.html',
                    controller: 'TireSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                publicPrice: null,
                                insurancePrice: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('tire-supplier', null, { reload: 'tire-supplier' });
                }, function() {
                    $state.go('tire-supplier');
                });
            }]
        })
        .state('tire-supplier.edit', {
            parent: 'tire-supplier',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-supplier/tire-supplier-dialog.html',
                    controller: 'TireSupplierDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TireSupplier', function(TireSupplier) {
                            return TireSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tire-supplier', null, { reload: 'tire-supplier' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('tire-supplier.delete', {
            parent: 'tire-supplier',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER','ROLE_ROSMI_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/tire-supplier/tire-supplier-delete-dialog.html',
                    controller: 'TireSupplierDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TireSupplier', function(TireSupplier) {
                            return TireSupplier.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('tire-supplier', null, { reload: 'tire-supplier' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
