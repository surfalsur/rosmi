(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('TireSupplierDetailController', TireSupplierDetailController);

    TireSupplierDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TireSupplier', 'Supplier', 'Tire'];

    function TireSupplierDetailController($scope, $rootScope, $stateParams, previousState, entity, TireSupplier, Supplier, Tire) {
        var vm = this;

        vm.tireSupplier = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('rosmiApp:tireSupplierUpdate', function(event, result) {
            vm.tireSupplier = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
