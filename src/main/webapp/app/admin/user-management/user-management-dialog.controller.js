(function() {
    'use strict';

    angular
        .module('rosmiApp')
        .controller('UserManagementDialogController',UserManagementDialogController);

    UserManagementDialogController.$inject = ['$stateParams', '$uibModalInstance', 'entity', 'User', 'JhiLanguageService'
    	,'Supplier','BranchOffice'];

    function UserManagementDialogController ($stateParams, $uibModalInstance, entity, User, JhiLanguageService,Supplier,BranchOffice) {
        var vm = this;
        
        loadSuppliers();
        loadBranchOffice();
        
        vm.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        vm.clear = clear;
        vm.languages = null;
        vm.save = save;
        vm.user = entity;
        vm.suppliers = null;
        vm.branchOffices = null;
        
        
        function loadSuppliers () {
        	
        	Supplier.query({
            }, onSuccess, onError);
        	
        	function onSuccess(data, headers) {
                vm.suppliers = data;
            }
        	
            function onError(error) {
                AlertService.error(error.data.message);
            }
           
        }
        
        
        function loadBranchOffice () {
        	
        	BranchOffice.query({
            }, onSuccess, onError);
        	
        	function onSuccess(data, headers) {
        		vm.branchOffices = data;
            }
        	
            function onError(error) {
                AlertService.error(error.data.message);
            }
           
        }
        

        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function onSaveSuccess (result) {
            vm.isSaving = false;
            $uibModalInstance.close(result);
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function save () {
            vm.isSaving = true;
            if (vm.user.id !== null) {
                User.update(vm.user, onSaveSuccess, onSaveError);
            } else {
                User.save(vm.user, onSaveSuccess, onSaveError);
            }
        }
    }
})();
