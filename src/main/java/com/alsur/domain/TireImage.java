package com.alsur.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TireImage.
 */
@Entity
@Table(name = "tire_image")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tireimage")
public class TireImage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "image_path",unique=true)
    private String imagePath;

    @ManyToOne
    private Tire tire;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public TireImage imagePath(String imagePath) {
        this.imagePath = imagePath;
        return this;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Tire getTire() {
        return tire;
    }

    public TireImage tire(Tire tire) {
        this.tire = tire;
        return this;
    }

    public void setTire(Tire tire) {
        this.tire = tire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TireImage tireImage = (TireImage) o;
        if (tireImage.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tireImage.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TireImage{" +
            "id=" + id +
            ", imagePath='" + imagePath + "'" +
            '}';
    }
}
