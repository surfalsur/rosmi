package com.alsur.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tire.
 */
@Entity
@Table(name = "tire")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tire")
public class Tire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "size")
    private String size;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "tire_branch_office",
               joinColumns = @JoinColumn(name="tires_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="branch_offices_id", referencedColumnName="ID"))
    private Set<BranchOffice> branchOffices = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "tire_car_model",
               joinColumns = @JoinColumn(name="tires_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="car_models_id", referencedColumnName="ID"))
    private Set<CarModel> carModels = new HashSet<>();
    
    @OneToMany(mappedBy = "tire")
    private Set<TireSupplier> suppliers = new HashSet<TireSupplier>();

    @ManyToOne
    private TireBrand tireBrand;
    
    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String imagePreviewPath;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Tire name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public Tire size(String size) {
        this.size = size;
        return this;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Set<BranchOffice> getBranchOffices() {
        return branchOffices;
    }

    public Tire branchOffices(Set<BranchOffice> branchOffices) {
        this.branchOffices = branchOffices;
        return this;
    }

    public Tire addBranchOffice(BranchOffice branchOffice) {
        branchOffices.add(branchOffice);
        return this;
    }

    public Tire removeBranchOffice(BranchOffice branchOffice) {
        branchOffices.remove(branchOffice);
        return this;
    }

    public void setBranchOffices(Set<BranchOffice> branchOffices) {
        this.branchOffices = branchOffices;
    }

    public Set<CarModel> getCarModels() {
        return carModels;
    }

    public Tire carModels(Set<CarModel> carModels) {
        this.carModels = carModels;
        return this;
    }

    public Tire addCarModel(CarModel carModel) {
        carModels.add(carModel);
        return this;
    }

    public Tire removeCarModel(CarModel carModel) {
        carModels.remove(carModel);
        return this;
    }

    public void setCarModels(Set<CarModel> carModels) {
        this.carModels = carModels;
    }

    public TireBrand getTireBrand() {
        return tireBrand;
    }

    public Tire tireBrand(TireBrand tireBrand) {
        this.tireBrand = tireBrand;
        return this;
    }

    public void setTireBrand(TireBrand tireBrand) {
        this.tireBrand = tireBrand;
    }

    
    
    public Set<TireSupplier> getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(Set<TireSupplier> suppliers) {
		this.suppliers = suppliers;
	}
	
	

	public String getImagePreviewPath() {
		return imagePreviewPath;
	}

	public void setImagePreviewPath(String imagePreviewPath) {
		this.imagePreviewPath = imagePreviewPath;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tire tire = (Tire) o;
        if (tire.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tire.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tire{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", size='" + size + "'" +
            '}';
    }
}
