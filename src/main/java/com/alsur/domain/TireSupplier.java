package com.alsur.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A TireSupplier.
 */
@Entity
@Table(name = "tire_supplier")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tiresupplier")
public class TireSupplier implements Serializable {

    private static final long serialVersionUID = 1L;

    
    @Embeddable
    public static class TireSupplierId implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
    	
		@Column(name = "supplier_id")
		protected Long supplierId;

		@Column(name = "tire_id")
		protected Long tireId;

		public TireSupplierId() {
			
		}

		public TireSupplierId(Long supplierId, Long tireId) {
			
			this.supplierId = supplierId;
			this.tireId = tireId;
		}
		
		
		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
		    }
		    if (o == null || getClass() != o.getClass()) {
		        return false;
		    }
		    TireSupplierId tireSupplierId = (TireSupplierId) o;
		    if (tireSupplierId.supplierId == this.supplierId && tireSupplierId.tireId == this.tireId) {
		    	return true;
		    }
		    return false;
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(tireId+supplierId);
		}
		
		@Override
		public String toString() {

			return String.valueOf(this.supplierId).concat(String.valueOf(this.tireId));
		}

		public Long getSupplierId() {
			return supplierId;
		}

		public Long getTireId() {
			return tireId;
		}
		
		
    	
    }
    
    
    
    @EmbeddedId
    private TireSupplierId id;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "public_price", precision=10, scale=2, nullable = false)
    private BigDecimal publicPrice;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "insurance_price", precision=10, scale=2, nullable = false)
    private BigDecimal insurancePrice;

    @ManyToOne
    @JoinColumn(name = "supplier_id", insertable = false, updatable = false)
    private Supplier supplier;

    @ManyToOne
    @JoinColumn(name = "tire_id", insertable = false, updatable = false)
    private Tire tire;

   
    public TireSupplier(){
    	
    }
    

    public TireSupplier(BigDecimal publicPrice, BigDecimal insurancePrice, Supplier supplier,
			Tire tire) {
		
		this.id = new TireSupplierId(supplier.getId(),tire.getId());
		this.publicPrice = publicPrice;
		this.insurancePrice = insurancePrice;
	}

    
	public TireSupplierId getId() {
		return id;
	}

	public void setId(TireSupplierId id) {
		this.id = id;
	}

	public BigDecimal getPublicPrice() {
        return publicPrice;
    }

    public TireSupplier publicPrice(BigDecimal publicPrice) {
        this.publicPrice = publicPrice;
        return this;
    }

    public void setPublicPrice(BigDecimal publicPrice) {
        this.publicPrice = publicPrice;
    }

    public BigDecimal getInsurancePrice() {
        return insurancePrice;
    }

    public TireSupplier insurancePrice(BigDecimal insurancePrice) {
        this.insurancePrice = insurancePrice;
        return this;
    }

    public void setInsurancePrice(BigDecimal insurancePrice) {
        this.insurancePrice = insurancePrice;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public TireSupplier supplier(Supplier supplier) {
        this.supplier = supplier;
        return this;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Tire getTire() {
        return tire;
    }

    public TireSupplier tire(Tire tire) {
        this.tire = tire;
        return this;
    }

    public void setTire(Tire tire) {
        this.tire = tire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TireSupplier tireSupplier = (TireSupplier) o;
        if (tireSupplier.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tireSupplier.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TireSupplier{" +
            "id=" + id +
            ", publicPrice='" + publicPrice + "'" +
            ", insurancePrice='" + insurancePrice + "'" +
            '}';
    }
}
