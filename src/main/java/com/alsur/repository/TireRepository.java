package com.alsur.repository;

import com.alsur.domain.Tire;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Tire entity.
 */

public interface TireRepository extends JpaRepository<Tire,Long> {



	public static final String FIND_TIRES_BY_PARAMS_PAGING =
			"select distinct tire from Tire tire "
    		+ "left join tire.branchOffices as branchOffice "
    		+ "left join tire.carModels as carModel "
    		+ "left join tire.suppliers as tireSupplier "
    		+ "left join tire.tireBrand as tireBrand "
    		+ "where (branchOffice.id = :branchOfficeId or :branchOfficeId is null) "
    		+ "and (carModel.id = :carModelId or :carModelId is null) "
    		+ "and (tireSupplier.id.supplierId = :supplierId or :supplierId is null) "
    		+ "and (tire.tireBrand.id = :tireBrandId or :tireBrandId is null) "
    		+ "and (LOWER(tire.name) LIKE CONCAT(LOWER(:tireName),'%') or :tireName is null) ";



	public static final String COUNT_TIRES_BY_PARAMS =
			"select count(tire.id) from Tire tire "
		    		+ "left join tire.branchOffices as branchOffice "
		    		+ "left join tire.carModels as carModel "
		    		+ "left join tire.suppliers as tireSupplier "
		    		+ "left join tire.tireBrand as tireBrand "
		    		+ "where (branchOffice.id = :branchOfficeId or :branchOfficeId is null) "
		    		+ "and (carModel.id = :carModelId or :carModelId is null) "
		    		+ "and (tireSupplier.id.supplierId = :supplierId or :supplierId is null) "
		    		+ "and (tire.tireBrand.id = :tireBrandId or :tireBrandId is null) "
		    		+ "and (LOWER(tire.name) LIKE CONCAT(LOWER(:tireName),'%') or :tireName is null) ";



    @Query("select distinct tire from Tire tire left join fetch tire.branchOffices left join fetch tire.carModels")
    List<Tire> findAllWithEagerRelationships();


    @Query("select tire from Tire tire left join fetch tire.branchOffices left join fetch tire.carModels where tire.id =:id")
    Tire findOneWithEagerRelationships(@Param("id") Long id);


    @Query(value=FIND_TIRES_BY_PARAMS_PAGING,
    		countQuery=COUNT_TIRES_BY_PARAMS)
    Page<Tire> findAllWithParams(@Param("branchOfficeId") Long branchOfficeId,
    		@Param("carModelId") Long carModelId,
    		@Param("supplierId") Long supplierId,
    		@Param("tireBrandId") Long tireBrandId,
    		@Param("tireName") String tireName,
    		Pageable pageable);




}
