package com.alsur.repository;

import com.alsur.domain.CarBrand;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CarBrand entity.
 */
@SuppressWarnings("unused")
public interface CarBrandRepository extends JpaRepository<CarBrand,Long> {

}
