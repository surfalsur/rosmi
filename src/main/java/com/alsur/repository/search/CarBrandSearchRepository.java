package com.alsur.repository.search;

import com.alsur.domain.CarBrand;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CarBrand entity.
 */
public interface CarBrandSearchRepository extends ElasticsearchRepository<CarBrand, Long> {
}
