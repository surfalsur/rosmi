package com.alsur.repository.search;

import com.alsur.domain.TireImage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TireImage entity.
 */
public interface TireImageSearchRepository extends ElasticsearchRepository<TireImage, Long> {
}
