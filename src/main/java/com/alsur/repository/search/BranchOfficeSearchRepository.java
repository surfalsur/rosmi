package com.alsur.repository.search;

import com.alsur.domain.BranchOffice;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the BranchOffice entity.
 */
public interface BranchOfficeSearchRepository extends ElasticsearchRepository<BranchOffice, Long> {
}
