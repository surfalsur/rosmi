package com.alsur.repository.search;

import com.alsur.domain.TireBrand;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TireBrand entity.
 */
public interface TireBrandSearchRepository extends ElasticsearchRepository<TireBrand, Long> {
}
