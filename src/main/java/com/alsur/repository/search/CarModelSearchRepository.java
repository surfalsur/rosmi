package com.alsur.repository.search;

import com.alsur.domain.CarModel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CarModel entity.
 */
public interface CarModelSearchRepository extends ElasticsearchRepository<CarModel, Long> {
}
