package com.alsur.repository.search;

import com.alsur.domain.TireSupplier;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TireSupplier entity.
 */
public interface TireSupplierSearchRepository extends ElasticsearchRepository<TireSupplier, Long> {
}
