package com.alsur.repository.search;

import com.alsur.domain.Tire;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Tire entity.
 */
public interface TireSearchRepository extends ElasticsearchRepository<Tire, Long> {
}
