package com.alsur.repository;

import com.alsur.domain.TireSupplier;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TireSupplier entity.
 */
@SuppressWarnings("unused")
public interface TireSupplierRepository extends JpaRepository<TireSupplier,Long> {

}
