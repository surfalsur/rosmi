package com.alsur.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alsur.domain.TireImage;
import java.lang.String;
import java.util.List;

/**
 * Spring Data JPA repository for the TireImage entity.
 */

public interface TireImageRepository extends JpaRepository<TireImage,Long> {

	
	List<TireImage> findByImagePath(String imagepath);
	
	
	List<TireImage> findByTireId(Long tireId);
}
