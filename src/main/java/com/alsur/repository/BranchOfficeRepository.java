package com.alsur.repository;

import com.alsur.domain.BranchOffice;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the BranchOffice entity.
 */
@SuppressWarnings("unused")
public interface BranchOfficeRepository extends JpaRepository<BranchOffice,Long> {

}
