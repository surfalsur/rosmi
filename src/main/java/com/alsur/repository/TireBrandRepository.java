package com.alsur.repository;

import com.alsur.domain.TireBrand;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TireBrand entity.
 */
@SuppressWarnings("unused")
public interface TireBrandRepository extends JpaRepository<TireBrand,Long> {

}
