package com.alsur.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alsur.domain.BranchOffice;
import com.alsur.repository.BranchOfficeRepository;
import com.alsur.repository.search.BranchOfficeSearchRepository;
import com.alsur.web.rest.util.HeaderUtil;
import com.alsur.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing BranchOffice.
 */
@RestController
@RequestMapping("/api")
public class BranchOfficeResource {

    private final Logger log = LoggerFactory.getLogger(BranchOfficeResource.class);
        
    @Inject
    private BranchOfficeRepository branchOfficeRepository;

    @Inject
    private BranchOfficeSearchRepository branchOfficeSearchRepository;

    /**
     * POST  /branch-offices : Create a new branchOffice.
     *
     * @param branchOffice the branchOffice to create
     * @return the ResponseEntity with status 201 (Created) and with body the new branchOffice, or with status 400 (Bad Request) if the branchOffice has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/branch-offices")
    @Timed
    public ResponseEntity<BranchOffice> createBranchOffice(@Valid @RequestBody BranchOffice branchOffice) throws URISyntaxException {
        log.debug("REST request to save BranchOffice : {}", branchOffice);
        if (branchOffice.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("branchOffice", "idexists", "A new branchOffice cannot already have an ID")).body(null);
        }
        BranchOffice result = branchOfficeRepository.save(branchOffice);
        branchOfficeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/branch-offices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("branchOffice", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /branch-offices : Updates an existing branchOffice.
     *
     * @param branchOffice the branchOffice to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated branchOffice,
     * or with status 400 (Bad Request) if the branchOffice is not valid,
     * or with status 500 (Internal Server Error) if the branchOffice couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/branch-offices")
    @Timed
    public ResponseEntity<BranchOffice> updateBranchOffice(@Valid @RequestBody BranchOffice branchOffice) throws URISyntaxException {
        log.debug("REST request to update BranchOffice : {}", branchOffice);
        if (branchOffice.getId() == null) {
            return createBranchOffice(branchOffice);
        }
        BranchOffice result = branchOfficeRepository.save(branchOffice);
        branchOfficeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("branchOffice", branchOffice.getId().toString()))
            .body(result);
    }

    /**
     * GET  /branch-offices : get all the branchOffices.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of branchOffices in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/branch-offices")
    @Timed
    public ResponseEntity<List<BranchOffice>> getAllBranchOffices(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of BranchOffices");
        Page<BranchOffice> page = branchOfficeRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/branch-offices");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /branch-offices/:id : get the "id" branchOffice.
     *
     * @param id the id of the branchOffice to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the branchOffice, or with status 404 (Not Found)
     */
    @GetMapping("/branch-offices/{id}")
    @Timed
    public ResponseEntity<BranchOffice> getBranchOffice(@PathVariable Long id) {
        log.debug("REST request to get BranchOffice : {}", id);
        BranchOffice branchOffice = branchOfficeRepository.findOne(id);
        return Optional.ofNullable(branchOffice)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /branch-offices/:id : delete the "id" branchOffice.
     *
     * @param id the id of the branchOffice to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/branch-offices/{id}")
    @Timed
    public ResponseEntity<Void> deleteBranchOffice(@PathVariable Long id) {
        log.debug("REST request to delete BranchOffice : {}", id);
        branchOfficeRepository.delete(id);
        branchOfficeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("branchOffice", id.toString())).build();
    }

    /**
     * SEARCH  /_search/branch-offices?query=:query : search for the branchOffice corresponding
     * to the query.
     *
     * @param query the query of the branchOffice search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/branch-offices")
    @Timed
    public ResponseEntity<List<BranchOffice>> searchBranchOffices(@RequestParam String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of BranchOffices for query {}", query);
        Page<BranchOffice> page = branchOfficeSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/branch-offices");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
