/**
 * View Models used by Spring MVC REST controllers.
 */
package com.alsur.web.rest.vm;
