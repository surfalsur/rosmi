package com.alsur.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alsur.domain.CarModel;
import com.alsur.repository.CarModelRepository;
import com.alsur.repository.search.CarModelSearchRepository;
import com.alsur.web.rest.util.HeaderUtil;
import com.alsur.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing CarModel.
 */
@RestController
@RequestMapping("/api")
public class CarModelResource {

    private final Logger log = LoggerFactory.getLogger(CarModelResource.class);
        
    @Inject
    private CarModelRepository carModelRepository;

    @Inject
    private CarModelSearchRepository carModelSearchRepository;

    /**
     * POST  /car-models : Create a new carModel.
     *
     * @param carModel the carModel to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carModel, or with status 400 (Bad Request) if the carModel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/car-models")
    @Timed
    public ResponseEntity<CarModel> createCarModel(@Valid @RequestBody CarModel carModel) throws URISyntaxException {
        log.debug("REST request to save CarModel : {}", carModel);
        if (carModel.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("carModel", "idexists", "A new carModel cannot already have an ID")).body(null);
        }
        CarModel result = carModelRepository.save(carModel);
        carModelSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/car-models/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("carModel", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /car-models : Updates an existing carModel.
     *
     * @param carModel the carModel to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated carModel,
     * or with status 400 (Bad Request) if the carModel is not valid,
     * or with status 500 (Internal Server Error) if the carModel couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/car-models")
    @Timed
    public ResponseEntity<CarModel> updateCarModel(@Valid @RequestBody CarModel carModel) throws URISyntaxException {
        log.debug("REST request to update CarModel : {}", carModel);
        if (carModel.getId() == null) {
            return createCarModel(carModel);
        }
        CarModel result = carModelRepository.save(carModel);
        carModelSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("carModel", carModel.getId().toString()))
            .body(result);
    }

    /**
     * GET  /car-models : get all the carModels.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of carModels in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/car-models")
    @Timed
    public ResponseEntity<List<CarModel>> getAllCarModels(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of CarModels");
        Page<CarModel> page = carModelRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/car-models");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /car-models/:id : get the "id" carModel.
     *
     * @param id the id of the carModel to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the carModel, or with status 404 (Not Found)
     */
    @GetMapping("/car-models/{id}")
    @Timed
    public ResponseEntity<CarModel> getCarModel(@PathVariable Long id) {
        log.debug("REST request to get CarModel : {}", id);
        CarModel carModel = carModelRepository.findOne(id);
        return Optional.ofNullable(carModel)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /car-models/:id : delete the "id" carModel.
     *
     * @param id the id of the carModel to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/car-models/{id}")
    @Timed
    public ResponseEntity<Void> deleteCarModel(@PathVariable Long id) {
        log.debug("REST request to delete CarModel : {}", id);
        carModelRepository.delete(id);
        carModelSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("carModel", id.toString())).build();
    }

    /**
     * SEARCH  /_search/car-models?query=:query : search for the carModel corresponding
     * to the query.
     *
     * @param query the query of the carModel search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/car-models")
    @Timed
    public ResponseEntity<List<CarModel>> searchCarModels(@RequestParam String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of CarModels for query {}", query);
        Page<CarModel> page = carModelSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/car-models");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
