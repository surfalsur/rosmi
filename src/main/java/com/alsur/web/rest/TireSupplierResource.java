package com.alsur.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alsur.domain.TireSupplier;
import com.alsur.domain.TireSupplier.TireSupplierId;
import com.alsur.repository.TireSupplierRepository;
import com.alsur.repository.search.TireSupplierSearchRepository;
import com.alsur.web.rest.util.HeaderUtil;
import com.alsur.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing TireSupplier.
 */
@RestController
@RequestMapping("/api")
public class TireSupplierResource {

    private final Logger log = LoggerFactory.getLogger(TireSupplierResource.class);
        
    @Inject
    private TireSupplierRepository tireSupplierRepository;

    @Inject
    private TireSupplierSearchRepository tireSupplierSearchRepository;

    /**
     * POST  /tire-suppliers : Create a new tireSupplier.
     *
     * @param tireSupplier the tireSupplier to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tireSupplier, or with status 400 (Bad Request) if the tireSupplier has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tire-suppliers")
    @Timed
    public ResponseEntity<TireSupplier> createTireSupplier(@Valid @RequestBody TireSupplier tireSupplier) throws URISyntaxException {
        log.debug("REST request to save TireSupplier : {}", tireSupplier);
        
        tireSupplier.setId(new TireSupplierId(tireSupplier.getSupplier().getId(),tireSupplier.getTire().getId()));
        
        TireSupplier result = tireSupplierRepository.save(tireSupplier);
        
        return ResponseEntity.created(new URI("/api/tire-suppliers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tireSupplier", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tire-suppliers : Updates an existing tireSupplier.
     *
     * @param tireSupplier the tireSupplier to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tireSupplier,
     * or with status 400 (Bad Request) if the tireSupplier is not valid,
     * or with status 500 (Internal Server Error) if the tireSupplier couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tire-suppliers")
    @Timed
    public ResponseEntity<TireSupplier> updateTireSupplier(@Valid @RequestBody TireSupplier tireSupplier) throws URISyntaxException {
        log.debug("REST request to update TireSupplier : {}", tireSupplier);
        if (tireSupplier.getId() == null) {
            return createTireSupplier(tireSupplier);
        }
        TireSupplier result = tireSupplierRepository.save(tireSupplier);
//        tireSupplierSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tireSupplier", tireSupplier.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tire-suppliers : get all the tireSuppliers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tireSuppliers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/tire-suppliers")
    @Timed
    public ResponseEntity<List<TireSupplier>> getAllTireSuppliers(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TireSuppliers");
        Page<TireSupplier> page = tireSupplierRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tire-suppliers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tire-suppliers/:id : get the "id" tireSupplier.
     *
     * @param id the id of the tireSupplier to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tireSupplier, or with status 404 (Not Found)
     */
    @GetMapping("/tire-suppliers/{id}")
    @Timed
    public ResponseEntity<TireSupplier> getTireSupplier(@PathVariable Long id) {
        log.debug("REST request to get TireSupplier : {}", id);
        TireSupplier tireSupplier = tireSupplierRepository.findOne(id);
        return Optional.ofNullable(tireSupplier)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tire-suppliers/:id : delete the "id" tireSupplier.
     *
     * @param id the id of the tireSupplier to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tire-suppliers/{id}")
    @Timed
    public ResponseEntity<Void> deleteTireSupplier(@PathVariable Long id) {
        log.debug("REST request to delete TireSupplier : {}", id);
        tireSupplierRepository.delete(id);
        tireSupplierSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tireSupplier", id.toString())).build();
    }

    /**
     * SEARCH  /_search/tire-suppliers?query=:query : search for the tireSupplier corresponding
     * to the query.
     *
     * @param query the query of the tireSupplier search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/tire-suppliers")
    @Timed
    public ResponseEntity<List<TireSupplier>> searchTireSuppliers(@RequestParam String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TireSuppliers for query {}", query);
        Page<TireSupplier> page = tireSupplierSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/tire-suppliers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
