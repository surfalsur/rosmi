package com.alsur.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alsur.domain.CarBrand;
import com.alsur.repository.CarBrandRepository;
import com.alsur.repository.search.CarBrandSearchRepository;
import com.alsur.web.rest.util.HeaderUtil;
import com.alsur.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing CarBrand.
 */
@RestController
@RequestMapping("/api")
public class CarBrandResource {

    private final Logger log = LoggerFactory.getLogger(CarBrandResource.class);
        
    @Inject
    private CarBrandRepository carBrandRepository;

    @Inject
    private CarBrandSearchRepository carBrandSearchRepository;

    /**
     * POST  /car-brands : Create a new carBrand.
     *
     * @param carBrand the carBrand to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carBrand, or with status 400 (Bad Request) if the carBrand has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/car-brands")
    @Timed
    public ResponseEntity<CarBrand> createCarBrand(@RequestBody CarBrand carBrand) throws URISyntaxException {
        log.debug("REST request to save CarBrand : {}", carBrand);
        if (carBrand.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("carBrand", "idexists", "A new carBrand cannot already have an ID")).body(null);
        }
        CarBrand result = carBrandRepository.save(carBrand);
        carBrandSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/car-brands/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("carBrand", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /car-brands : Updates an existing carBrand.
     *
     * @param carBrand the carBrand to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated carBrand,
     * or with status 400 (Bad Request) if the carBrand is not valid,
     * or with status 500 (Internal Server Error) if the carBrand couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/car-brands")
    @Timed
    public ResponseEntity<CarBrand> updateCarBrand(@RequestBody CarBrand carBrand) throws URISyntaxException {
        log.debug("REST request to update CarBrand : {}", carBrand);
        if (carBrand.getId() == null) {
            return createCarBrand(carBrand);
        }
        CarBrand result = carBrandRepository.save(carBrand);
        carBrandSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("carBrand", carBrand.getId().toString()))
            .body(result);
    }

    /**
     * GET  /car-brands : get all the carBrands.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of carBrands in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/car-brands")
    @Timed
    public ResponseEntity<List<CarBrand>> getAllCarBrands(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of CarBrands");
        Page<CarBrand> page = carBrandRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/car-brands");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /car-brands/:id : get the "id" carBrand.
     *
     * @param id the id of the carBrand to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the carBrand, or with status 404 (Not Found)
     */
    @GetMapping("/car-brands/{id}")
    @Timed
    public ResponseEntity<CarBrand> getCarBrand(@PathVariable Long id) {
        log.debug("REST request to get CarBrand : {}", id);
        CarBrand carBrand = carBrandRepository.findOne(id);
        return Optional.ofNullable(carBrand)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /car-brands/:id : delete the "id" carBrand.
     *
     * @param id the id of the carBrand to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/car-brands/{id}")
    @Timed
    public ResponseEntity<Void> deleteCarBrand(@PathVariable Long id) {
        log.debug("REST request to delete CarBrand : {}", id);
        carBrandRepository.delete(id);
        carBrandSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("carBrand", id.toString())).build();
    }

    /**
     * SEARCH  /_search/car-brands?query=:query : search for the carBrand corresponding
     * to the query.
     *
     * @param query the query of the carBrand search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/car-brands")
    @Timed
    public ResponseEntity<List<CarBrand>> searchCarBrands(@RequestParam String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of CarBrands for query {}", query);
        Page<CarBrand> page = carBrandSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/car-brands");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
