package com.alsur.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alsur.domain.TireBrand;
import com.alsur.repository.TireBrandRepository;
import com.alsur.repository.search.TireBrandSearchRepository;
import com.alsur.web.rest.util.HeaderUtil;
import com.alsur.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing TireBrand.
 */
@RestController
@RequestMapping("/api")
public class TireBrandResource {

    private final Logger log = LoggerFactory.getLogger(TireBrandResource.class);
        
    @Inject
    private TireBrandRepository tireBrandRepository;

    @Inject
    private TireBrandSearchRepository tireBrandSearchRepository;

    /**
     * POST  /tire-brands : Create a new tireBrand.
     *
     * @param tireBrand the tireBrand to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tireBrand, or with status 400 (Bad Request) if the tireBrand has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tire-brands")
    @Timed
    public ResponseEntity<TireBrand> createTireBrand(@RequestBody TireBrand tireBrand) throws URISyntaxException {
        log.debug("REST request to save TireBrand : {}", tireBrand);
        if (tireBrand.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tireBrand", "idexists", "A new tireBrand cannot already have an ID")).body(null);
        }
        TireBrand result = tireBrandRepository.save(tireBrand);
        tireBrandSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tire-brands/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tireBrand", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tire-brands : Updates an existing tireBrand.
     *
     * @param tireBrand the tireBrand to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tireBrand,
     * or with status 400 (Bad Request) if the tireBrand is not valid,
     * or with status 500 (Internal Server Error) if the tireBrand couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tire-brands")
    @Timed
    public ResponseEntity<TireBrand> updateTireBrand(@RequestBody TireBrand tireBrand) throws URISyntaxException {
        log.debug("REST request to update TireBrand : {}", tireBrand);
        if (tireBrand.getId() == null) {
            return createTireBrand(tireBrand);
        }
        TireBrand result = tireBrandRepository.save(tireBrand);
        tireBrandSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tireBrand", tireBrand.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tire-brands : get all the tireBrands.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tireBrands in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/tire-brands")
    @Timed
    public ResponseEntity<List<TireBrand>> getAllTireBrands(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TireBrands");
        Page<TireBrand> page = tireBrandRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tire-brands");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tire-brands/:id : get the "id" tireBrand.
     *
     * @param id the id of the tireBrand to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tireBrand, or with status 404 (Not Found)
     */
    @GetMapping("/tire-brands/{id}")
    @Timed
    public ResponseEntity<TireBrand> getTireBrand(@PathVariable Long id) {
        log.debug("REST request to get TireBrand : {}", id);
        TireBrand tireBrand = tireBrandRepository.findOne(id);
        return Optional.ofNullable(tireBrand)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tire-brands/:id : delete the "id" tireBrand.
     *
     * @param id the id of the tireBrand to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tire-brands/{id}")
    @Timed
    public ResponseEntity<Void> deleteTireBrand(@PathVariable Long id) {
        log.debug("REST request to delete TireBrand : {}", id);
        tireBrandRepository.delete(id);
        tireBrandSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tireBrand", id.toString())).build();
    }

    /**
     * SEARCH  /_search/tire-brands?query=:query : search for the tireBrand corresponding
     * to the query.
     *
     * @param query the query of the tireBrand search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/tire-brands")
    @Timed
    public ResponseEntity<List<TireBrand>> searchTireBrands(@RequestParam String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TireBrands for query {}", query);
        Page<TireBrand> page = tireBrandSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/tire-brands");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
