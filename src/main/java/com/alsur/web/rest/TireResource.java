package com.alsur.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alsur.domain.Tire;
import com.alsur.service.ImageService;
import com.alsur.service.TireService;
import com.alsur.web.rest.util.HeaderUtil;
import com.alsur.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Tire.
 */
@RestController
@RequestMapping("/api")
public class TireResource {

    private final Logger log = LoggerFactory.getLogger(TireResource.class);
        
    @Inject
    private TireService tireService;
    
    @Inject
    private ImageService imageService;

    /**
     * POST  /tires : Create a new tire.
     *
     * @param tire the tire to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tire, or with status 400 (Bad Request) if the tire has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
//    @PostMapping("/tires")
//    @Timed
//    public ResponseEntity<Tire> createTire(@RequestBody Tire tire) throws URISyntaxException {
//        log.debug("REST request to save Tire : {}", tire);
//        if (tire.getId() != null) {
//            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tire", "idexists", "A new tire cannot already have an ID")).body(null);
//        }
//        Tire result = tireService.save(tire);
//        return ResponseEntity.created(new URI("/api/tires/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert("tire", result.getId().toString()))
//            .body(result);
//    }

    /**
     * PUT  /tires : Updates an existing tire.
     *
     * @param tire the tire to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tire,
     * or with status 400 (Bad Request) if the tire is not valid,
     * or with status 500 (Internal Server Error) if the tire couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tires")
    @Timed
    public ResponseEntity<Tire> updateTire(@RequestBody Tire tire,HttpServletRequest request) throws URISyntaxException {
        log.debug("REST request to update Tire : {}", tire);
        if (tire.getId() == null) {
            return createTireWithImages(tire,null,request);
        }
        Tire result = tireService.save(tire);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tire", tire.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tires : get all the tires.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tires in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/tires")
    @Timed
    public ResponseEntity<List<Tire>> getAllTires(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tires");
        Page<Tire> page = tireService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tires");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tires/:id : get the "id" tire.
     *
     * @param id the id of the tire to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tire, or with status 404 (Not Found)
     */
    @GetMapping("/tires/{id}")
    @Timed
    public ResponseEntity<Tire> getTire(@PathVariable Long id) {
        log.debug("REST request to get Tire : {}", id);
        Tire tire = tireService.findOne(id);
        return Optional.ofNullable(tire)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tires/:id : delete the "id" tire.
     *
     * @param id the id of the tire to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tires/{id}")
    @Timed
    public ResponseEntity<Void> deleteTire(@PathVariable Long id) {
        log.debug("REST request to delete Tire : {}", id);
        tireService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tire", id.toString())).build();
    }

    /**
     * SEARCH  /_search/tires?query=:query : search for the tire corresponding
     * to the query.
     *
     * @param query the query of the tire search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/tires")
    @Timed
    public ResponseEntity<List<Tire>> searchTires(@RequestParam String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Tires for query {}", query);
        Page<Tire> page = tireService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/tires");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    
    
    /**
     * GET  /tires : get all the tires by params.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tires in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(params={"branchOfficeId","carModelId","supplierId","tireBrandId","tireName"},
    method=RequestMethod.GET,value="/tires")
    @Timed
    public ResponseEntity<List<Tire>> getAllTiresWithParams(@RequestParam("branchOfficeId") Long branchOfficeId,
    		@RequestParam("carModelId") Long carModelId,
    		@RequestParam("supplierId") Long supplierId,
    		@RequestParam("tireBrandId") Long tireBrandId,
    		@RequestParam("tireName") String tireName,
    		@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tires");
        Page<Tire> page = tireService.findAllWithParams(branchOfficeId, carModelId, supplierId, tireBrandId, tireName, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tires");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    
    
    
    /**
         * POST  /tires : Create a new tire.
         *
         * @param tire the tire to create
         * @return the ResponseEntity with status 201 (Created) and with body the new tire, or with status 400 (Bad Request) if the tire has already an ID
         * @throws URISyntaxException if the Location URI syntax is incorrect
         */
        
        @PostMapping("/tires")
        @Timed
        public ResponseEntity<Tire> createTireWithImages(@RequestPart(value = "tire") Tire tire,
        		@RequestPart(value = "files", required = false) List<MultipartFile> files,
        		HttpServletRequest request) throws URISyntaxException {
        	
        	
            log.debug("REST request to save Tire : {}", tire);
            
            
            if (tire.getId() != null) {
                return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tire", "idexists", "A new tire cannot already have an ID")).body(null);
            }
            Tire result = tireService.save(tire);
            
            String realPath = request.getServletContext().getRealPath("/");
            StringBuilder imagesBaserealPath = new StringBuilder();
            imagesBaserealPath.append(realPath);
            imagesBaserealPath.append("/files/tire");
            imageService.saveImages(files, tire,imagesBaserealPath.toString());
            
            
            return ResponseEntity.created(new URI("/api/tires/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("tire", result.getId().toString()))
                .body(result);
        }
    
    

}
