package com.alsur.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alsur.domain.TireImage;
import com.alsur.repository.TireImageRepository;
import com.alsur.repository.search.TireImageSearchRepository;
import com.alsur.service.ImageService;
import com.alsur.web.rest.util.HeaderUtil;
import com.alsur.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing TireImage.
 */
@RestController
@RequestMapping("/api")
public class TireImageResource {

    private final Logger log = LoggerFactory.getLogger(TireImageResource.class);
        
    @Inject
    private TireImageRepository tireImageRepository;

    @Inject
    private TireImageSearchRepository tireImageSearchRepository;
    
    @Inject
    private ImageService imageService;

    /**
     * POST  /tire-images : Create a new tireImage.
     *
     * @param tireImage the tireImage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tireImage, or with status 400 (Bad Request) if the tireImage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tire-images")
    @Timed
    public ResponseEntity<TireImage> createTireImage(@RequestBody TireImage tireImage) throws URISyntaxException {
        log.debug("REST request to save TireImage : {}", tireImage);
        if (tireImage.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tireImage", "idexists", "A new tireImage cannot already have an ID")).body(null);
        }
        TireImage result = tireImageRepository.save(tireImage);
        tireImageSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tire-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tireImage", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tire-images : Updates an existing tireImage.
     *
     * @param tireImage the tireImage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tireImage,
     * or with status 400 (Bad Request) if the tireImage is not valid,
     * or with status 500 (Internal Server Error) if the tireImage couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tire-images")
    @Timed
    public ResponseEntity<TireImage> updateTireImage(@RequestBody TireImage tireImage) throws URISyntaxException {
        log.debug("REST request to update TireImage : {}", tireImage);
        if (tireImage.getId() == null) {
            return createTireImage(tireImage);
        }
        TireImage result = tireImageRepository.save(tireImage);
        tireImageSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tireImage", tireImage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tire-images : get all the tireImages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tireImages in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/tire-images")
    @Timed
    public ResponseEntity<List<TireImage>> getAllTireImages(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TireImages");
        Page<TireImage> page = tireImageRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tire-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tire-images/:id : get the "id" tireImage.
     *
     * @param id the id of the tireImage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tireImage, or with status 404 (Not Found)
     */
    @GetMapping("/tire-images/{id}")
    @Timed
    public ResponseEntity<TireImage> getTireImage(@PathVariable Long id) {
        log.debug("REST request to get TireImage : {}", id);
        TireImage tireImage = tireImageRepository.findOne(id);
        return Optional.ofNullable(tireImage)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tire-images/:id : delete the "id" tireImage.
     *
     * @param id the id of the tireImage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tire-images/{id}")
    @Timed
    public ResponseEntity<Void> deleteTireImage(@PathVariable Long id) {
        log.debug("REST request to delete TireImage : {}", id);
        tireImageRepository.delete(id);
        tireImageSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tireImage", id.toString())).build();
    }

    /**
     * SEARCH  /_search/tire-images?query=:query : search for the tireImage corresponding
     * to the query.
     *
     * @param query the query of the tireImage search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_search/tire-images")
    @Timed
    public ResponseEntity<List<TireImage>> searchTireImages(@RequestParam String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TireImages for query {}", query);
        Page<TireImage> page = tireImageSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/tire-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    
    
    



}
