package com.alsur.util;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	
	
	public static boolean convert(MultipartFile file,String newFileName)  {
		
	    try {
			file.transferTo(new File(newFileName));
			return true;
		} catch (IllegalStateException | IOException e) {
			
			return false;
		}
	    
	}
	
	
	public static Optional<String> getExtension(String contentType) {
		
		if(contentType !=null && contentType.length() > 0) {
			String [] contentTypes = contentType.split("/");
			if(contentTypes != null && contentTypes.length > 1) {
				String extension = contentTypes[1];
				return Optional.of(extension);
			}
			 
		}
		 
		return Optional.empty();
	}
}
