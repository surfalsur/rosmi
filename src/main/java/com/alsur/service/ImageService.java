package com.alsur.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alsur.domain.Tire;
import com.alsur.domain.TireImage;
import com.alsur.repository.TireImageRepository;

@Service
public class ImageService {
	
	private final Logger log = LoggerFactory.getLogger(ImageService.class);
	
	
	
	@Inject
	private TireImageRepository tireImageRepository;
	/***
	 * Save images for specific model and id. Create a folder for every new id or get folder if exists
	 * @param files
	 * @param clazzName
	 * @param id
	 */
	public void saveImages(List<MultipartFile> files,Tire tire,final String realPath) {
		
		
		try {
			
			StringBuilder pathModelIdPathFolder = new StringBuilder(realPath);
			pathModelIdPathFolder.append("/");
			pathModelIdPathFolder.append(String.valueOf(tire.getId()));
			
			
			
			File modelFolder = new File(pathModelIdPathFolder.toString());
			if(!modelFolder.exists()) {
				FileUtils.forceMkdir(modelFolder);
				
			}
			
			//saves images
            files.stream().forEach(mpi -> {
    				
    				TireImage tireImage = new TireImage();
    				tireImage.setTire(tire);
    				tireImage.setImagePath("");
    				tireImage = tireImageRepository.save(tireImage);
    				
    				
    				//store image in file system
    				
    				//TODO fix this depending on the browser path (just name or complete path)
    				
    				
    				
    				StringBuilder fullPathImageBuilder =  new StringBuilder();
    				fullPathImageBuilder.append(pathModelIdPathFolder.toString());
    				fullPathImageBuilder.append("/");
    				fullPathImageBuilder.append(mpi.getOriginalFilename());
    				
    					
    				if(com.alsur.util.FileUtils.convert(mpi, fullPathImageBuilder.toString())) {
    						
    					StringBuilder relativePathImage = new StringBuilder();
        				relativePathImage.append("/files/tire/");
        				relativePathImage.append(String.valueOf(tire.getId()));
        				relativePathImage.append("/");
        				relativePathImage.append(mpi.getOriginalFilename());
    					
    					tireImage.setImagePath(relativePathImage.toString());
    					tireImageRepository.save(tireImage);
    					
    				}
    				
    		});
			
		}catch(Exception e) {
			log.error("Error on try to save images for tireId: "+tire.getId());
		}
		
		
		
	}
	
	
	public InputStream getImage(Long tireId) {
		
		List<TireImage> images = tireImageRepository.findByTireId(tireId);
		
		
		images.stream().findFirst().map(image -> {
				try {
					return new FileInputStream(FileUtils.getFile(image.getImagePath()));
				} catch (FileNotFoundException e) {
					
					return null;
					
				}
			});
		
		
		
		return null;
	}
}
