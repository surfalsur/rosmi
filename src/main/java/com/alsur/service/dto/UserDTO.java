package com.alsur.service.dto;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.alsur.config.Constants;
import com.alsur.domain.Authority;
import com.alsur.domain.BranchOffice;
import com.alsur.domain.Supplier;
import com.alsur.domain.User;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 100)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 100)
    private String email;

    private boolean activated = false;

    @Size(min = 2, max = 5)
    private String langKey;

    private Set<String> authorities;
    
    private Supplier supplier;
    
    private BranchOffice branchOffice;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this(user.getLogin(), user.getFirstName(), user.getLastName(),
            user.getEmail(), user.getActivated(), user.getLangKey(),
            user.getAuthorities().stream().map(Authority::getName)
                .collect(Collectors.toSet()),user.getSupplier(),user.getBranchOffice());
    }
    
    

    public UserDTO(String login, String firstName, String lastName,
            String email, boolean activated, String langKey, Set<String> authorities,Supplier supplier,
            BranchOffice branchOffice) {

            this.login = login;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.activated = activated;
            this.langKey = langKey;
            this.authorities = authorities;
            this.supplier = supplier;
            this.branchOffice = branchOffice;
        }
    
    
    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActivated() {
        return activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }
    
    

    public Supplier getSupplier() {
		return supplier;
	}
    
    

	public BranchOffice getBranchOffice() {
		return branchOffice;
	}

	@Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", authorities=" + authorities +'\'' +
            ", supplier='" + supplier +'\'' +
            ", branchOffice='" + branchOffice +
            "}";
    }
}
