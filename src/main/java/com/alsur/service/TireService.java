package com.alsur.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alsur.domain.Tire;
import com.alsur.repository.TireImageRepository;
import com.alsur.repository.TireRepository;
import com.alsur.repository.search.TireSearchRepository;

/**
 * Service Implementation for managing Tire.
 */
@Service
@Transactional
public class TireService {

    private final Logger log = LoggerFactory.getLogger(TireService.class);
    
    @Inject
    private TireRepository tireRepository;

    @Inject
    private TireSearchRepository tireSearchRepository;
    
    @Inject
    private TireImageRepository tireImageRepository;

    /**
     * Save a tire.
     *
     * @param tire the entity to save
     * @return the persisted entity
     */
    public Tire save(Tire tire) {
        log.debug("Request to save Tire : {}", tire);
        Tire result = tireRepository.save(tire);
        tireSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the tires.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Tire> findAll(Pageable pageable) {
        log.debug("Request to get all Tires");
        Page<Tire> result = tireRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one tire by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Tire findOne(Long id) {
        log.debug("Request to get Tire : {}", id);
        Tire tire = tireRepository.findOneWithEagerRelationships(id);
        return tire;
    }

    /**
     *  Delete the  tire by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Tire : {}", id);
        tireRepository.delete(id);
        tireSearchRepository.delete(id);
    }

    /**
     * Search for the tire corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Tire> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Tires for query {}", query);
        Page<Tire> result = tireSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
    
    
    
    /**
     * Search for the tire corresponding to the query.
     *
     *  @param("branchOfficeId") Long branchOfficeId,
    	@param("carModelId") Long carModelId,
    	@param("supplierId") Long supplierId,
    	@param("tireBrandId") Long tireBrandId,
    	@param("tireName") String tireName,
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Tire> findAllWithParams(Long branchOfficeId,
    		Long carModelId,
    		Long supplierId,
    		Long tireBrandId,
    		String tireName,
    		Pageable pageable) {
        
        Page<Tire> result = tireRepository.findAllWithParams(branchOfficeId, carModelId, supplierId, tireBrandId, tireName,
        		pageable);
        
        result.forEach(tire -> 
        	tire.setImagePreviewPath(tireImageRepository.findByTireId(tire.getId()).stream().findFirst()
        			.get().getImagePath()));
        		
        
        return result;
    }
}
